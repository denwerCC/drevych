<?php
// Heading
$_['heading_title']          	= 'Новини - список записів';

// Text
$_['text_success']          	= 'Список записів оновлено!';
$_['text_list']          		= 'Список записів';
$_['text_add']          		= 'Додавання запису';
$_['text_edit']         	 	= 'Редагування запису';
$_['text_default']        		= 'Основний магазин';

// Column
$_['column_image']          	= 'Зображення';
$_['column_name']          		= 'Назва статті';
$_['column_category']         	= 'Відображати в категорії';
$_['column_status']          	= 'Статус';
$_['column_date_available']    	= 'Дата публикації';
$_['column_date_modified']     	= 'Дата зміни';
$_['column_action']          	= 'Дії';

// Entry
$_['entry_date_available']      = 'Дата публікації:';
$_['entry_status']          	= 'Статус:';
$_['entry_sort_order']          = 'Порядок сортування:';

$_['entry_name']          		= 'Назва:';
$_['entry_preview']         	= 'Опис для анонсу:';
$_['entry_description']         = 'Повний текст статті:';
$_['entry_meta_title']          = 'HTML-тег Title';
$_['entry_meta_h1']             = 'HTML-тег H1';
$_['entry_meta_description']    = 'Мета-тег Description:';
$_['entry_meta_keyword']        = 'Мета-тег Keywords:';
$_['entry_tag']          		= 'Тег запису:';

$_['entry_image']          		= 'Зображення запису:';
$_['entry_keyword']          	= 'SEO URL:';

$_['entry_main_category']       = 'Головна категорія:';
$_['entry_category']          	= 'Відображати в категоріях:';
$_['entry_store']          		= 'Магазини:';
$_['entry_related']          	= 'Схожі статті:';
$_['entry_related_products']	= 'Рекомендовані продукти:';

$_['entry_attribute']          	= 'Атрибут:';
$_['entry_attribute_group']     = 'Група атрибутів:';
$_['entry_text']          	= 'Текст:';

$_['entry_layout']          	= 'Перевизначити темплейт:';

// Help
$_['help_keyword']          	= 'Замініть пробіли на тире. Повинно бути унікальним на всю систему.';
$_['help_tag']          	= 'розділяються комою';

// Error
$_['error_warning']          	= 'Уважно перевірте орфографічні помилки!';
$_['error_keyword']          	= 'Цей SEO keyword вже використовується!';
$_['error_permission']          = 'У Вас немає прав на зміну матеріалу!';
$_['error_name']          		= 'Назва статті повина бути від 3 до 255 символів!';
$_['error_meta_title']          = 'Мета-тег Title повинен бути від 3 до 255 символів!';
$_['error_model']          		= 'Модель запису повиннна бути віл 3 до 64 символів!';