<?php
// Heading
$_['heading_title']          		= 'Новини - список категорій';

// Text
$_['text_success']          		= 'Список категорій оновлено!';
$_['text_list']          		= 'Список категорій';
$_['text_add']          		= 'Додавання категорії';
$_['text_edit']          		= 'Редагування категорії';
$_['text_default']          		= 'Основной магазин';
$_['text_update_success']         	= 'БД оновлені до актуальної версії!';

// Column
$_['column_name']          		= 'Назва категорії';
$_['column_count_elements']		= 'Кількість матеріальів';
$_['column_sort_order']          	= 'Порядок сортування';
$_['column_action']          		= 'Дії';

// Entry
$_['entry_name']          		= 'Назва категорії:';
$_['entry_description']          	= 'Опис';
$_['entry_meta_title']          	= 'HTML-тег Title';
$_['entry_meta_h1']                     = 'HTML-тег H1';
$_['entry_meta_description']            = 'Мета-тег Description';
$_['entry_meta_keyword']          	= 'Мета-тег Keywords';

$_['entry_parent']          		= 'Батьківська категорія:';
$_['entry_store']          		= 'Магазини:';
$_['entry_keyword']          		= 'SEO URL:';
$_['entry_image']          		= 'Зображення категорії';
$_['entry_image_size']       		= 'Розмір зображення категорії:';
$_['entry_show_preview']          	= 'Показувати анонс повної статті:';
$_['entry_sort_order']          	= 'Порядок сортування:';
$_['entry_status']          		= 'Статус:';
$_['entry_limit']          		= 'Кількість матеріалів на сторінці:';
$_['entry_sort_by']          		= 'Сортування матеріалів:';
$_['entry_show_in_top']          	= 'Показувати категорії в верхньому меню:';
$_['entry_show_in_top_articles']        = 'Показувати матеріали в категорії верхнього меню:';
$_['entry_show_in_sitemap']       	= 'Показувати категорію на карті сайту:';
$_['entry_show_in_sitemap_articles']    = 'Показувати матеріали на карті сайту:';

$_['entry_template_category']       	= 'Шаблон виводу категорії:';
$_['entry_template_article']        	= 'Шаблон виводу статті:';
$_['entry_images_size']       		= 'Розмір зображення на сторінці категорії:';
$_['entry_images_size_articles_big']	= 'Розмір великого зображення на сторінці матеріалу:';
$_['entry_images_size_articles_small']	= 'Розмір малого зображення на сторінці матеріалу:';
$_['entry_date_format']       		= 'Формат дати в матеріалах:';
$_['entry_layout']          		= 'Виберіть макет:';

// Help
$_['help_keyword']          		= 'Замініть пробіли на тире. Повинно бути унікальним на усю систему.';
$_['help_limit']          		= 'Для того щоб не виводити матеріали, поставте 0.';
$_['help_template_category']            = 'Вкажіть ім\'я файлу з шаблону виводу матеріалів категорії. Файл повинен лежати в /catalog/view/theme/[template name]/template/newsblog/';
$_['help_template_article']             = 'Вкажіть ім\'я файлу з шаблону виводу детального опису статті. Файл повинен лежати в /catalog/view/theme/[template name]/template/newsblog/';
$_['help_date_format']         		= 'Повності відповідає формату функції date на php .<br />Залиште поле пустим, щоб не виводити дату<br />Основні значення:<br />d - день місяця, m - місяць, Y - рік, H - година, i - хвиилина';

// Error
$_['error_keyword']          		= 'Цей SEO keyword вже використовується!';
$_['error_meta_title']          	= 'Мета-тег Title повинен мати від 3 до 255 символів!';
$_['error_name']          		= 'Назва категорії повинна мати від 2 до 32 символів!';
$_['error_permission']          	= 'У Вас немає права на внесення змін в категорію!';
$_['error_warning']          		= 'Уважно перевірте форму на помилки!';

// Sort
$_['sort_by_sort_order']          	= 'по порядку сортування';
$_['sort_by_date_available']            = 'по даті публікації';
$_['sort_by_name']          		= 'по імені';
$_['sort_direction_desc']          	= 'по зменшенню';
$_['sort_direction_asc']          	= 'по збільшенню';

// Placeholder
$_['placeholder_template_category'] = 'За замовчуванням category.tpl';
$_['placeholder_template_article']  = 'За замовчуванням  article.tpl';
$_['placeholder_image_size_width'] 	= 'Ширина';
$_['placeholder_image_size_height']	= 'Висота';
$_['placeholder_date_format']		= 'd.m.Y H:i:s';