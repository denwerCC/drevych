<?php
// Heading
$_['heading_title']    = 'Самовивіз';

// Text
$_['text_shipping']    = 'Доставка';
$_['text_success']     = 'Успіх: Змінено самовивіз!';
$_['text_edit']        = 'Змінити самовивіз';

// Entry
$_['entry_geo_zone']   = 'Геозона';
$_['entry_status']     = 'Стан';
$_['entry_sort_order'] = 'Порядок сортування';

// Error
$_['error_permission'] = 'Попередження: Нема дозволу на зміну самовивіз!';