<?php

class ControllerCatalogSales extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('catalog/sales');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/sales');

        $this->getList();
    }

    public function add() {
        $this->load->language('catalog/sales');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/sales');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_sales->addSale($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('catalog/sales');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/sales');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_catalog_sales->editSale($this->request->get['sale_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('catalog/sales');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/sales');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $sales_total) {
                $this->model_catalog_sales->deleteSale($sales_total);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'id.title';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link('catalog/sales/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link('catalog/sales/delete', 'token=' . $this->session->data['token'] . $url, true);

        $data['sales'] = array();

        $filter_data = array(
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $sales_total = $this->model_catalog_sales->getTotalSales();

        $results = $this->model_catalog_sales->getSales($filter_data);
        foreach ($results as $result) {
            $data['sales'][] = array(
                'sale_id' => $result['sale_id'],
                'title' => $result['title'],
                'date_from' => $result['date_from'],
                'date_to' => $result['date_to'],
                'edit' => $this->url->link('catalog/sales/edit', 'token=' . $this->session->data['token'] . '&sale_id=' . $result['sale_id'] . $url, true)
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_title'] = $this->language->get('column_title');
        $data['column_sort_order'] = $this->language->get('column_sort_order');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');

        $data['column_date_from'] = $this->language->get('entry_date_from');
        $data['column_date_to'] = $this->language->get('entry_date_to');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array) $this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_title'] = $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . '&sort=id.title' . $url, true);
        $data['sort_sort_order'] = $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . '&sort=i.sort_order' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $sales_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($sales_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($sales_total - $this->config->get('config_limit_admin'))) ? $sales_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $sales_total, ceil($sales_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/sales_list', $data));
    }

    protected function getForm() {
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_form'] = !isset($this->request->get['sale_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['entry_title'] = $this->language->get('entry_title');
        $data['entry_description'] = $this->language->get('entry_description');
        $data['entry_meta_title'] = $this->language->get('entry_meta_title');
        $data['entry_meta_description'] = $this->language->get('entry_meta_description');
        $data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
        $data['entry_date_from'] = $this->language->get('entry_date_from');
        $data['entry_date_to'] = $this->language->get('entry_date_to');
        $data['entry_category'] = $this->language->get('entry_category');
        $data['entry_keyword'] = $this->language->get('entry_keyword');
        $data['entry_store'] = $this->language->get('entry_store');
        $data['entry_bottom'] = $this->language->get('entry_bottom');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['help_keyword'] = $this->language->get('help_keyword');
        $data['help_bottom'] = $this->language->get('help_bottom');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['tab_general'] = $this->language->get('tab_general');
        $data['tab_data'] = $this->language->get('tab_data');
        $data['entry_category_baner'] = $this->language->get('entry_category_baner');
        $data['entry_baner'] = $this->language->get('entry_baner');
        $data['entry_products'] = $this->language->get('entry_products');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['title'])) {
            $data['error_title'] = $this->error['title'];
        } else {
            $data['error_title'] = array();
        }

        if (isset($this->error['description'])) {
            $data['error_description'] = $this->error['description'];
        } else {
            $data['error_description'] = array();
        }

        if (isset($this->error['meta_title'])) {
            $data['error_meta_title'] = $this->error['meta_title'];
        } else {
            $data['error_meta_title'] = array();
        }

        if (isset($this->error['keyword'])) {
            $data['error_keyword'] = $this->error['keyword'];
        } else {
            $data['error_keyword'] = '';
        }

        if (isset($this->error['date_from'])) {
            $data['error_date_from'] = $this->error['date_from'];
        } else {
            $data['error_date_from'] = '';
        }

        if (isset($this->error['date_to'])) {
            $data['error_date_to'] = $this->error['date_to'];
        } else {
            $data['error_date_to'] = '';
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['sale_id'])) {
            $data['action'] = $this->url->link('catalog/sales/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('catalog/sales/edit', 'token=' . $this->session->data['token'] . '&sale_id=' . $this->request->get['sale_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('catalog/sales', 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['sale_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $sales_info = $this->model_catalog_sales->getSale($this->request->get['sale_id']);
        }
        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['sales_description'])) {
            $data['sales_description'] = $this->request->post['sales_description'];
        } elseif (isset($this->request->get['sale_id'])) {
            $data['sales_description'] = $this->model_catalog_sales->getSalesDescriptions($this->request->get['sale_id']);
        } else {
            $data['sales_description'] = array();
        }

        if (isset($this->request->post['keyword'])) {
            $data['keyword'] = $this->request->post['keyword'];
        } elseif (!empty($sales_info)) {
            $data['keyword'] = $sales_info['keyword'];
        } else {
            $data['keyword'] = '';
        }

        if (isset($this->request->post['date_from'])) {
            $data['date_from'] = $this->request->post['date_from'];
        } elseif (!empty($sales_info)) {
            $data['date_from'] = $sales_info['date_from'];
        } else {
            $data['date_from'] = '';
        }

        if (isset($this->request->post['date_to'])) {
            $data['date_to'] = $this->request->post['date_to'];
        } elseif (!empty($sales_info)) {
            $data['date_to'] = $sales_info['date_to'];
        } else {
            $data['date_to'] = '';
        }

        if (isset($this->request->post['category_id'])) {
            $data['category_id'] = $this->request->post['category_id'];
        } elseif (!empty($sales_info)) {
            $data['category_id'] = $sales_info['category_id'];
        } else {
            $data['category_id'] = '';
        }

        if (isset($this->request->post['baner'])) {
            $data['baner'] = $this->request->post['baner'];
        } elseif (!empty($sales_info)) {
            $data['baner'] = $sales_info['baner'];
        } else {
            $data['baner'] = '';
        }

        if (isset($this->request->post['baner_category'])) {
            $data['baner_category'] = $this->request->post['baner_category'];
        } elseif (!empty($sales_info)) {
            $data['baner_category'] = $sales_info['baner_category'];
        } else {
            $data['baner_category'] = '';
        }

        $this->load->model('tool/image');
        if (!empty($data['baner'])) {
            if ($data['baner'] && is_file(DIR_IMAGE . $data['baner'])) {
                $data['baner_thumb'] = $this->model_tool_image->resize($data['baner'], 100, 100);
            }
        }
        if (!empty($data['baner_category'])) {
            if ($data['baner_category'] && is_file(DIR_IMAGE . $data['baner_category'])) {
                $data['baner_category_thumb'] = $this->model_tool_image->resize($data['baner_category'], 100, 100);
            }
        }
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        if (isset($this->request->post['sales_products'])) {
            $products = $this->request->post['sales_products'];
        } elseif (isset($this->request->get['sale_id'])) {
            $products = $this->model_catalog_sales->getSaleProducts($this->request->get['sale_id']);
        } else {
            $products = array();
        }

        $data['sales_products'] = array();
        foreach ($products as $products_id) {
            $this->load->model('catalog/product');
            $product_info = $this->model_catalog_product->getProduct($products_id);

            if ($product_info) {
                $data['sales_products'][] = array(
                    'product_id' => $product_info['product_id'],
                    'name' => $product_info['name'] . '(' . $product_info['product_id'] . ')'
                );
            }
        }
        $this->load->model('catalog/sales');
        if ($this->model_catalog_sales->getParentCategories()) {
            $data['categories'] = $this->model_catalog_sales->getParentCategories();
        } else {
            $data['categories'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/sales_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/sales')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['sales_description'] as $language_id => $value) {
            if ((utf8_strlen($value['title']) < 3) || (utf8_strlen($value['title']) > 64)) {
                $this->error['title'][$language_id] = $this->language->get('error_title');
            }

            if (utf8_strlen($value['description']) < 3) {
                $this->error['description'][$language_id] = $this->language->get('error_description');
            }

            if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
                $this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
            }
        }
        if (utf8_strlen($this->request->post['keyword']) > 0) {
            $this->load->model('catalog/url_alias');

            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

            if ($url_alias_info && isset($this->request->get['sale_id']) && $url_alias_info['query'] != 'sale_id=' . $this->request->get['sale_id']) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

            if ($url_alias_info && !isset($this->request->get['sale_id'])) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }
        }

        if (empty($this->request->post['date_from']) || empty($this->request->post['date_to'])) {
            $this->error['date_from'] = 'Дата початку акції не може бути пустою';
            $this->error['date_to'] = 'Дата закінчення акції не може бути пустою';
        } else if (!empty($this->request->post['date_from']) && !empty($this->request->post['date_to'])) {
            $start = strtotime($this->request->post['date_from']);
            $end = strtotime($this->request->post['date_to']);
            if ($end < $start) {
                $this->error['date_to'] = 'Дата закінчення акції не може бути меншою дати початку';
            }
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/sales')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $this->load->model('setting/store');

        foreach ($this->request->post['selected'] as $sale_id) {
            if ($this->config->get('config_account_id') == $sale_id) {
                $this->error['warning'] = $this->language->get('error_account');
            }

            if ($this->config->get('config_checkout_id') == $sale_id) {
                $this->error['warning'] = $this->language->get('error_checkout');
            }

            if ($this->config->get('config_affiliate_id') == $sale_id) {
                $this->error['warning'] = $this->language->get('error_affiliate');
            }

            if ($this->config->get('config_return_id') == $sale_id) {
                $this->error['warning'] = $this->language->get('error_return');
            }
        }

        return !$this->error;
    }

    public function autocomplete() {
        $json = array();

        if (isset($this->request->get['product_name'])) {
            $this->load->model('catalog/product');

            $product_data = array(
                'filter_name' => $this->request->get['product_name'],
                'sort' => 'name',
                'order' => 'ASC',
                'start' => 0,
                'limit' => 8
            );

            $results = $this->model_catalog_product->getProducts($product_data);

            foreach ($results as $result) {
                $json[] = array(
                    'product_id' => $result['product_id'],
                    'name' => strip_tags(html_entity_decode($result['name'] . ' (' . $result['product_id'] . ')', ENT_QUOTES, 'UTF-8'))
                );
            }
        }

        $sort_order = array();

        foreach ($json as $key => $value) {
            $sort_order[$key] = $value['name'];
        }

        array_multisort($sort_order, SORT_ASC, $json);

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

}
