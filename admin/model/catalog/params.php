<?php

class ModelCatalogParams extends Model {

    public function addParams($data) {
        $visible = isset($data['visible']) && $data['visible'] == 'on' ? true : false;

        $this->db->query("INSERT INTO `" . DB_PREFIX . "params_filter_group` SET sort_order = '" . (int) $data['sort_order'] . "', custom_class = '" . $data['custom_class'] . "', secret_key = '" . $data['secret_key'] . "', visible = '" . $visible . "'");

        $params_group_id = $this->db->getLastId();

        foreach ($data['params_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter_group_description SET params_group_id = '" . (int) $params_group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        if (isset($data['params'])) {
            foreach ($data['params'] as $params) {
                $params['default'] = isset($params['default']) && $params['default'] == 'on' ? true : false;

                $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter SET params_group_id = '" . (int) $params_group_id . "', sort_order = '" . (int) $params['sort_order'] . "', back_color = '" . $params['back_color'] . "', `default` = '" . $params['default'] . "', `value` = '" . $params['value'] . "'");

                $params_id = $this->db->getLastId();

                if (isset($params['back_image'])) {
                    $this->db->query("UPDATE " . DB_PREFIX . "params_filter SET back_image = '" . $this->db->escape($params['back_image']) . "' WHERE params_id = '" . (int) $params_id . "'");
                }

                foreach ($params['params_description'] as $language_id => $params_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter_description SET params_id = '" . (int) $params_id . "', language_id = '" . (int) $language_id . "', params_group_id = '" . (int) $params_group_id . "', name = '" . $this->db->escape($params_description['name']) . "'");
                }
            }
        }

        return $params_group_id;
    }

    public function editParams($params_group_id, $data) {
        $visible = isset($data['visible']) && $data['visible'] == 'on' ? true : false;

        $this->db->query("UPDATE `" . DB_PREFIX . "params_filter_group` SET sort_order = '" . (int) $data['sort_order'] . "', custom_class = '" . $data['custom_class'] . "', secret_key = '" . $data['secret_key'] . "', visible = '" . $visible . "' WHERE params_group_id = '" . (int) $params_group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "params_filter_group_description WHERE params_group_id = '" . (int) $params_group_id . "'");

        foreach ($data['params_group_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter_group_description SET params_group_id = '" . (int) $params_group_id . "', language_id = '" . (int) $language_id . "', name = '" . $this->db->escape($value['name']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "params_filter WHERE params_group_id = '" . (int) $params_group_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "params_filter_description WHERE params_group_id = '" . (int) $params_group_id . "'");

        if (isset($data['params'])) {
            foreach ($data['params'] as $params) {
                $params['default'] = isset($params['default']) && $params['default'] == 'on' ? true : false;

                if ($params['params_id']) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter SET params_id = '" . (int) $params['params_id'] . "', params_group_id = '" . (int) $params_group_id . "', sort_order = '" . (int) $params['sort_order'] . "', back_color = '" . $params['back_color'] . "', `default` = '" . $params['default'] . "', `value` = '" . $params['value'] . "'");
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter SET params_group_id = '" . (int) $params_group_id . "', sort_order = '" . (int) $params['sort_order'] . "', back_color = '" . $params['back_color'] . "', `default` = '" . $params['default'] . "', `value` = '" . $params['value'] . "'");
                }

                $params_id = $this->db->getLastId();

                if (isset($params['back_image'])) {
                    $this->db->query("UPDATE " . DB_PREFIX . "params_filter SET back_image = '" . $this->db->escape($params['back_image']) . "' WHERE params_id = '" . (int) $params_id . "'");
                }

                foreach ($params['params_description'] as $language_id => $params_description) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "params_filter_description SET params_id = '" . (int) $params_id . "', language_id = '" . (int) $language_id . "', params_group_id = '" . (int) $params_group_id . "', name = '" . $this->db->escape($params_description['name']) . "'");
                }
                
                $language_id = (int) $this->config->get('config_language_id');
                $keyQuery = $this->db->query("SELECT * FROM `" . DB_PREFIX . "option` op LEFT JOIN " . DB_PREFIX . "option_value opv ON (op.option_id = opv.option_id) LEFT JOIN " . DB_PREFIX . "option_value_description opvd ON (opv.option_value_id = opvd.option_value_id) WHERE op.secret_key = '" . $data['secret_key'] . "' AND opvd.name = '" . $params['params_description'][$language_id]['name'] . "'");
                if ($keyQuery->num_rows) {
                    $price = str_replace(',', '.', $params['value']);
                    $price = number_format($price, 4);
                    $option_value_id = $keyQuery->row['option_value_id'];
                    $this->db->query("UPDATE `" . DB_PREFIX . "product_option_value` SET price = '" . $price . "' WHERE option_value_id = '" . (int) $option_value_id . "'");
                }
            }
        }
    }

    public function deleteParams($params_group_id) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "params_filter_group` WHERE params_group_id = '" . (int) $params_group_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "params_filter_group_description` WHERE params_group_id = '" . (int) $params_group_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "params_filter` WHERE params_group_id = '" . (int) $params_group_id . "'");
        $this->db->query("DELETE FROM `" . DB_PREFIX . "params_filter_description` WHERE params_group_id = '" . (int) $params_group_id . "'");
    }

    public function getParamsGroup($params_group_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "params_filter_group` fg LEFT JOIN " . DB_PREFIX . "params_filter_group_description fgd ON (fg.params_group_id = fgd.params_group_id) WHERE fg.params_group_id = '" . (int) $params_group_id . "' AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getParamsGroups($data = array()) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "params_filter_group` fg LEFT JOIN " . DB_PREFIX . "params_filter_group_description fgd ON (fg.params_group_id = fgd.params_group_id) WHERE fgd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        $sort_data = array(
            'fgd.name',
            'fg.sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY fgd.name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getParamsGroupDescriptions($params_group_id) {
        $params_group_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "params_filter_group_description WHERE params_group_id = '" . (int) $params_group_id . "'");

        foreach ($query->rows as $result) {
            $params_group_data[$result['language_id']] = array('name' => $result['name']);
        }

        return $params_group_data;
    }

    //public function getFilter($filter_id) {
    public function getParameter($params_id) {
        $query = $this->db->query("SELECT *, (SELECT name FROM " . DB_PREFIX . "params_filter_group_description fgd WHERE f.params_group_id = fgd.params_group_id AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS `group` FROM " . DB_PREFIX . "params_filter f LEFT JOIN " . DB_PREFIX . "params_filter_description fd ON (f.params_id = fd.params_id) WHERE f.params_id = '" . (int) $params_id . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getParams($data) {
        $sql = "SELECT *, (SELECT name FROM " . DB_PREFIX . "params_filter_group_description fgd WHERE f.params_group_id = fgd.params_group_id AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "') AS `group` FROM " . DB_PREFIX . "params_filter f LEFT JOIN " . DB_PREFIX . "params_filter_description fd ON (f.params_id = fd.params_id) WHERE fd.language_id = '" . (int) $this->config->get('config_language_id') . "'";

        if (!empty($data['params_name'])) {
            $sql .= " AND fd.name LIKE '" . $this->db->escape($data['params_name']) . "%'";
        }

        $sql .= " ORDER BY f.sort_order ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getParamsDescriptions($params_group_id) {
        $params_data = array();

        $params_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "params_filter WHERE params_group_id = '" . (int) $params_group_id . "'");

        foreach ($params_query->rows as $params) {
            $params_description_data = array();

            $params_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "params_filter_description WHERE params_id = '" . (int) $params['params_id'] . "'");

            foreach ($params_description_query->rows as $params_description) {
                $params_description_data[$params_description['language_id']] = array('name' => $params_description['name']);
            }

            $params_data[] = array(
                'params_id' => $params['params_id'],
                'params_description' => $params_description_data,
                'sort_order' => $params['sort_order'],
                'back_color' => $params['back_color'],
                'back_image' => $params['back_image'],
                'default' => $params['default'],
                'value' => $params['value']
            );
        }

        return $params_data;
    }

    public function getTotalParamsGroups() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "params_filter_group`");

        return $query->row['total'];
    }

}
