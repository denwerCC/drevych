<?php

class ModelCatalogSales extends Model {

    public function addSale($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "sales "
                . "SET date_from = '" . $this->db->escape($data['date_from']) . "', "
                . "date_to = '" . $this->db->escape($data['date_to']) . "', "
                . "category_id = '" . (int) $data['category_id'] . "', "
                . "baner = '" . $data['baner'] . "', "
                . "baner_category = '" . $data['baner_category'] . "'");

        $sale_id = $this->db->getLastId();
        foreach ($data['sales_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "sales_description SET sale_id = '" . (int) $sale_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "title = '" . $this->db->escape($value['title']) . "', "
                    . "description = '" . $this->db->escape($value['description']) . "', "
                    . "meta_title = '" . $this->db->escape($value['meta_title']) . "', "
                    . "meta_description = '" . $this->db->escape($value['meta_description']) . "', "
                    . "meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        foreach ($data['sales_products'] as $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "sales_products SET sale_id = '" . (int) $sale_id . "', "
                    . "product_id = '" . (int) $value . "'");
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'sale_id=" . (int) $sale_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('sales');
        return $sale_id;
    }

    public function editSale($sale_id, $data) {
        
        $this->db->query("UPDATE " . DB_PREFIX . "sales "
                . "SET date_from = '" . $this->db->escape($data['date_from']) . "', "
                . "date_to = '" . $this->db->escape($data['date_to']) . "', "
                . "category_id = '" . (int) $data['category_id'] . "', "
                . "baner = '" . $data['baner'] . "', "
                . "baner_category = '" . $data['baner_category'] . "' "
                . "WHERE id = '" . (int) $sale_id . "'");
        
        $this->db->query("DELETE FROM " . DB_PREFIX . "sales_description WHERE sale_id = '" . (int) $sale_id . "'");

        foreach ($data['sales_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "sales_description "
                    . "SET sale_id = '" . (int) $sale_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "title = '" . $this->db->escape($value['title']) . "', "
                    . "description = '" . $this->db->escape($value['description']) . "', "
                    . "meta_title = '" . $this->db->escape($value['meta_title']) . "', "
                    . "meta_description = '" . $this->db->escape($value['meta_description']) . "', "
                    . "meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "sales_products WHERE sale_id = '" . (int) $sale_id . "'");
        
        foreach ($data['sales_products'] as $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "sales_products SET sale_id = '" . (int) $sale_id . "', "
                    . "product_id = '" . (int) $value . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'sale_id=" . (int) $sale_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'sale_id=" . (int) $sale_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('sales');
    }

    public function deleteSale($sale_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "sales WHERE id = '" . (int) $sale_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "sales_description WHERE sale_id = '" . (int) $sale_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "sales_products WHERE sale_id = '" . (int) $sale_id . "'");

        $this->cache->delete('sales');
    }

    public function getSale($sale_id) {
        $query = $this->db->query("SELECT DISTINCT * ,(SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'sale_id=" . (int)$sale_id . "') AS keyword "
                . " FROM " . DB_PREFIX . "sales AS sale"
                . " WHERE `sale`.`id` = '" . (int) $sale_id . "'");
        return $query->row;
    }

    public function getSales($data = array()) {
        if ($data) {
            
            $sql = "SELECT * FROM `" . DB_PREFIX . "sales`AS `main` "
                    //. "LEFT JOIN `" . DB_PREFIX . "sales_products` `prod` ON (`main`.`id` = `prod`.`sale_id`) "
                    . "LEFT JOIN `" . DB_PREFIX . "sales_description` `desc` ON (`main`.`id` = `desc`.`sale_id`) "
                    . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'";

            $sort_data = array(
                'main.date_from',
                'main.date_to'
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY main.date_from";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $sales_data = $this->cache->get('sales.' . (int) $this->config->get('config_language_id'));

            if (!$sales_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales i "
                        . " LEFT JOIN " . DB_PREFIX . "sales_description id ON (i.id = id.sale_id) "
                        . " WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                        . " ORDER BY id.title");

                $sales_data = $query->rows;

                $this->cache->set('sales.' . (int) $this->config->get('config_language_id'), $sales_data);
            }

            return $sales_data;
        }
    }

    public function getSalesDescriptions($sale_id) {
        $sales_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_description WHERE sale_id = '" . (int) $sale_id . "'");

        foreach ($query->rows as $result) {
            $sales_description_data[$result['language_id']] = array(
                'title' => $result['title'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword']
            );
        }

        return $sales_description_data;
    }

    public function getTotalSales() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sales");

        return $query->row['total'];
    }

    public function getParentCategories() {
        $query = $this->db->query("SELECT `cat`.`category_id`, `desc`.`name`, `cat`.`parent_id`, `cat`.`status` FROM `" . DB_PREFIX . "category` `cat` "
                . "LEFT JOIN `" . DB_PREFIX . "category_description` `desc` ON (`cat`.`category_id` = `desc`.`category_id`) "
                . "WHERE `cat`.`parent_id` = '0' AND `cat`.`status` = '1' AND `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getSaleProducts($sale_id) {
        $sale_data = array();

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "sales_products` WHERE `sale_id` = '" . (int) $sale_id . "'");
        foreach ($query->rows as $result) {
            $sale_data[] = $result['product_id'];
        }

        return $sale_data;
    }

}
