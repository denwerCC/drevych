<?php

class ModelDesignGallery extends Model {

    public function addGallery($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "gallery "
                . "SET category_id = '" . (int) $data['category_id'] . "', "
                . "banner = '" . $data['banner'] . "'");

        $gallery_id = $this->db->getLastId();
        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_description SET gallery_id = '" . (int) $gallery_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "title = '" . $this->db->escape($value['title']) . "', "
                    . "button_href = '" . $this->db->escape($value['button_href']) . "', "
                    . "price_text = '" . $this->db->escape($value['price_text']) . "', "
                    . "description = '" . $this->db->escape($value['description']) . "', "
                    . "short_description = '" . $this->db->escape($value['short_description']) . "', "
                    . "meta_title = '" . $this->db->escape($value['meta_title']) . "', "
                    . "meta_description = '" . $this->db->escape($value['meta_description']) . "', "
                    . "meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        foreach ($data['gallery_photos'] as $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_photo SET gallery_id = '" . (int) $gallery_id . "', "
                    . "sort_order = '" . (int) $value['sort_order'] . "', "
                    . "photo = '" . $value['photo'] . "'");
        }

        if (isset($data['keyword'])) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id = " . (int) $gallery_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('gallery');
        return $gallery_id;
    }

    public function editGallery($gallery_id, $data) {

        $this->db->query("UPDATE " . DB_PREFIX . "gallery "
                . "SET category_id = '" . (int) $data['category_id'] . "', "
                . "banner = '" . $data['banner'] . "'"
                . "WHERE id = '" . (int) $gallery_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");

        foreach ($data['gallery_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_description SET gallery_id = '" . (int) $gallery_id . "', "
                    . "language_id = '" . (int) $language_id . "', "
                    . "title = '" . $this->db->escape($value['title']) . "', "
                    . "button_href = '" . $this->db->escape($value['button_href']) . "', "
                    . "price_text = '" . $this->db->escape($value['price_text']) . "', "
                    . "description = '" . $this->db->escape($value['description']) . "', "
                    . "short_description = '" . $this->db->escape($value['short_description']) . "', "
                    . "meta_title = '" . $this->db->escape($value['meta_title']) . "', "
                    . "meta_description = '" . $this->db->escape($value['meta_description']) . "', "
                    . "meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_photo WHERE gallery_id = '" . (int) $gallery_id . "'");

        foreach ($data['gallery_photos'] as $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "gallery_photo SET gallery_id = '" . (int) $gallery_id . "', "
                    . "sort_order = '" . (int) $value['sort_order'] . "', "
                    . "photo = '" . $value['photo'] . "'");
        }

        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int) $gallery_id . "'");

        if ($data['keyword']) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'gallery_id=" . (int) $gallery_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
        }

        $this->cache->delete('gallery');
    }

    public function deleteGallery($gallery_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery WHERE id = '" . (int) $gallery_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "gallery_products WHERE gallery_id = '" . (int) $gallery_id . "'");

        $this->cache->delete('gallery');
    }

    public function getGallery($gallery_id) {
        $query = $this->db->query("SELECT DISTINCT * ,(SELECT DISTINCT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'gallery_id=" . (int) $gallery_id . "') AS keyword "
                . " FROM " . DB_PREFIX . "gallery AS gallery"
                . " WHERE `gallery`.`id` = '" . (int) $gallery_id . "'");
        return $query->row;
    }

    public function getGalleries($data = array()) {
        if ($data) {

            $sql = "SELECT * FROM `" . DB_PREFIX . "gallery` AS `main` "
                    . "LEFT JOIN `" . DB_PREFIX . "gallery_description` `desc` ON (`main`.`id` = `desc`.`gallery_id`) "
                    . "LEFT JOIN `" . DB_PREFIX . "category_description` `cat` ON (`main`.`category_id` = `cat`.`category_id` AND `cat`.`language_id` = '" . (int) $this->config->get('config_language_id') . "') "
                    . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'";

            $sort_data = array(
                'main.category_id',
            );

            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY main.category_id";
            }

            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }

            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }

                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }

                $sql .= " LIMIT " . (int) $data['start'] . "," . (int) $data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $gallery_data = $this->cache->get('gallery.' . (int) $this->config->get('config_language_id'));

            if (!$gallery_data) {
                $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "gallery i "
                        . " LEFT JOIN " . DB_PREFIX . "gallery_description id ON (i.id = id.gallery_id) "
                        . " WHERE id.language_id = '" . (int) $this->config->get('config_language_id') . "' "
                        . " ORDER BY id.title");

                $gallery_data = $query->rows;

                $this->cache->set('gallery.' . (int) $this->config->get('config_language_id'), $gallery_data);
            }

            return $gallery_data;
        }
    }

    public function getGalleryDescriptions($gallery_id) {
        $gallery_description_data = array();

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "gallery_description WHERE gallery_id = '" . (int) $gallery_id . "'");

        foreach ($query->rows as $result) {
            $gallery_description_data[$result['language_id']] = array(
                'title' => $result['title'],
                'short_description' => $result['short_description'],
                'description' => $result['description'],
                'short_description' => $result['short_description'],
                'meta_title' => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword' => $result['meta_keyword'],
                'button_href' => $result['button_href'],
                'price_text' => $result['price_text']
            );
        }
        return $gallery_description_data;
    }

    public function getTotalGalleries() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "gallery");

        return $query->row['total'];
    }

    public function getParentCategories() {
        $query = $this->db->query("SELECT `cat`.`category_id`, `desc`.`name`, `cat`.`parent_id`, `cat`.`status` FROM `" . DB_PREFIX . "category` `cat` "
                . "LEFT JOIN `" . DB_PREFIX . "category_description` `desc` ON (`cat`.`category_id` = `desc`.`category_id`) "
                . "WHERE `cat`.`parent_id` = '0' AND `cat`.`status` = '1' AND `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->rows;
    }

    public function getGalleryPhotos($gallery_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "gallery_photo` WHERE `gallery_id` = '" . (int) $gallery_id . "' ORDER BY sort_order ASC");
        return $query->rows;
    }

}
