<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-gallery" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-gallery" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="gallery_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_short_description; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="gallery_description[<?php echo $language['language_id']; ?>][short_description]" value="<?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['short_description'] : ''; ?>" placeholder="<?php echo $entry_short_description; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />                                            
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="gallery_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            <?php if (isset($error_description[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="gallery_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="gallery_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="gallery_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_custom_button_href; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="gallery_description[<?php echo $language['language_id']; ?>][button_href]" value="<?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['button_href'] : ''; ?>" placeholder="<?php echo $entry_custom_button_href; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_custom_price_text; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="gallery_description[<?php echo $language['language_id']; ?>][price_text]" value="<?php echo isset($gallery_description[$language['language_id']]) ? $gallery_description[$language['language_id']]['price_text'] : ''; ?>" placeholder="<?php echo $entry_custom_price_text; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />                                            
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                                    <?php if ($error_keyword) { ?>
                                    <div class="text-danger"><?php echo $error_keyword; ?></div>
                                    <?php } ?>                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_category; ?></label>
                                <div class="col-sm-10">
                                    <select name="category_id" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($categories as $category) { ?>
                                        <?php if (isset($category_id) && $category_id == $category['category_id']) { ?>
                                        <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="banner"><span><?php echo $entry_banner; ?></span></label>
                                <div class="col-sm-10">
                                    <a href="" id="thumb-image-banner" data-toggle="image" class="img-thumbnail"><img src="<?php echo !empty($baner_thumb) ? $baner_thumb : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                    <input type="hidden" name="banner" value="<?php echo $banner ?>" id="input-image-banner" />              
                                </div>
                            </div>
                            <table id="gallery_photo" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <td class="text-right">Sort order</td>
                                        <td></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $photo_row = 0; ?>
                                    <?php foreach ($gallery_photos as $photo) { ?>
                                    <tr id="gallery-row<?php echo $photo_row; ?>">
                                        <td class="text-left" style="width: 70%;">
                                            <input type="hidden" name="gallery_photos[<?php echo $photo_row; ?>][gallery_id]" value="" />  
                                            <input type="text" name="gallery_photos[<?php echo $photo_row; ?>][sort_order]" value="<?php echo $photo['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />  
                                        </td>  
                                        <td class="text-right">
                                            <a href="" id="thumb-image<?php echo $photo_row; ?>"data-toggle="image" class="img-thumbnail">
                                                <img src="<?php echo !empty($photo['back_image_thumb']) ? $photo['back_image_thumb'] : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />
                                            </a>
                                            <input type="hidden" name="gallery_photos[<?php echo $photo_row; ?>][photo]" value="<?php echo $photo['photo']; ?>" id="input-image<?php echo $photo_row; ?>" />
                                        </td>  
                                        <td class="text-left">
                                            <button type="button" onclick="$('#gallery-row<?php echo $photo_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger">
                                                <i class="fa fa-minus-circle"></i>
                                            </button></td>
                                    </tr>
                                    <?php $photo_row++; ?>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left"><a onclick="addPhotoRow();" data-toggle="tooltip" title="<?=$add_button_photo ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>            
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('#language a:first').tab('show');

        var photo_row = <?php echo $photo_row; ?> ;
                function addPhotoRow() {
                    html = '<tr id="gallery-row' + photo_row + '">';
                    html += '  <td class="text-left" style="width: 70%;"><input type="hidden" name="gallery_photos[' + photo_row + '][gallery_id]" value="" />';
                    html += '  <input type="text" name="gallery_photos[' + photo_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />';
                    html += '  </td>';
                    html += '  <td class="text-right"><a href="" id="thumb-image' + photo_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>\n\
                               <input type="hidden" name="gallery_photos[' + photo_row + '][photo]" value="" id="input-image' + photo_row + '" /></td>';
                    html += '  <td class="text-left"><button type="button" onclick="$(\'#gallery-row' + photo_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
                    html += '  </tr>';
                    console.log(html);
                    $('#gallery_photo tbody').append(html);
                    photo_row++;
                }
    </script>
</div>
<?php echo $footer; ?>