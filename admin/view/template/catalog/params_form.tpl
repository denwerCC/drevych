<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-params" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-params" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label"><?php echo $entry_group; ?></label>
            <div class="col-sm-10">
              <?php foreach ($languages as $language) { ?>
              <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <input type="text" name="params_group_description[<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($params_group_description[$language['language_id']]) ? $params_group_description[$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_group; ?>" class="form-control" />
              </div>
              <?php if (isset($error_group[$language['language_id']])) { ?>
              <div class="text-danger"><?php echo $error_group[$language['language_id']]; ?></div>
              <?php } ?>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-custom-class">Custom class</label>
            <div class="col-sm-10">
              <input type="text" name="custom_class" value="<?php echo $custom_class; ?>" placeholder="Custom class" id="input-custom-class" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-secret-key">Secret key</label>
            <div class="col-sm-10">
              <input type="text" name="secret_key" value="<?php echo $secret_key; ?>" placeholder="Secret key" id="input-secret-key" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-visible">Visible</label>
            <div class="col-sm-10">
              <input type="checkbox" name="visible" <?php echo $visible ? 'checked' : ''; ?> id="input-visible" class="form-control" />
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
          <table id="params" class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <td class="text-left required"><?php echo $entry_name ?></td>
                <td class="text-right"><?php echo $entry_sort_order; ?></td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php $params_row = 0; ?>
              <?php foreach ($params as $parameter) { ?>
              <tr id="params-row<?php echo $params_row; ?>">
                <td class="text-left" style="width: 70%;"><input type="hidden" name="params[<?php echo $params_row; ?>][params_id]" value="<?php echo $parameter['params_id']; ?>" />
                  <?php foreach ($languages as $language) { ?>
                  <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                    <input type="text" name="params[<?php echo $params_row; ?>][params_description][<?php echo $language['language_id']; ?>][name]" value="<?php echo isset($parameter['params_description'][$language['language_id']]) ? $parameter['params_description'][$language['language_id']]['name'] : ''; ?>" placeholder="<?php echo $entry_name ?>" class="form-control" />
                  </div>
                  <?php if (isset($error_params[$params_row][$language['language_id']])) { ?>
                  <div class="text-danger"><?php echo $error_params[$params_row][$language['language_id']]; ?></div>
                  <?php } ?>
                  <?php } ?></td>
                <td class="text-left">
                    <input type="text" name="params[<?php echo $params_row; ?>][back_color]" value="<?php echo $parameter['back_color']; ?>" placeholder="Колір параметра" id="input-back-color<?php echo $params_row; ?>" class="form-control" />
                    <input type="text" name="params[<?php echo $params_row; ?>][sort_order]" value="<?php echo $parameter['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order<?php echo $params_row; ?>" class="form-control" />
                    <input type="text" name="params[<?php echo $params_row; ?>][value]" value="<?php echo $parameter['value']; ?>" placeholder="Значення" id="input-value<?php echo $params_row; ?>" class="form-control" />
                    <label class="control-label" for="input-default<?php echo $params_row; ?>">Default</label>
                    <input type="checkbox" name="params[<?php echo $params_row; ?>][default]" <?php echo $parameter['default'] ? 'checked' :  ''; ?> id="input-default<?php echo $params_row; ?>" class="form-control" />
                </td>              
            </div>
                <td class="text-right">
                    <a href="" id="thumb-image<?php echo $params_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo !empty($parameter['back_image_thumb']) ? $parameter['back_image_thumb'] : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                    <input type="hidden" name="params[<?php echo $params_row; ?>][back_image]" value="<?php echo $parameter['back_image']; ?>" id="input-image<?php echo $params_row; ?>" />
                <td class="text-left"><button type="button" onclick="$('#params-row<?php echo $params_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
              </tr>
              <?php $params_row++; ?>
              <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="2"></td>
                <td class="text-left"><a onclick="addParamsRow();" data-toggle="tooltip" title="<?php echo $button_params_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a></td>
              </tr>
            </tfoot>
          </table>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
var params_row = <?php echo $params_row; ?>;

function addParamsRow() {
	html  = '<tr id="params-row' + params_row + '">';	
    html += '  <td class="text-left" style="width: 70%;"><input type="hidden" name="params[' + params_row + '][params_id]" value="" />';
	<?php foreach ($languages as $language) { ?>
	html += '  <div class="input-group">';
	html += '    <span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span><input type="text" name="params[' + params_row + '][params_description][<?php echo $language['language_id']; ?>][name]" value="" placeholder="<?php echo $entry_name ?>" class="form-control" />';
    html += '  </div>';
	<?php } ?>
	html += '  </td>';
	html += '  <td class="text-left"><input type="text" name="params[' + params_row + '][back_color]" value="" placeholder="Колір параметра" id="input-back-color' + params_row + '" class="form-control" />\n\
                        <input type="text" name="params[' + params_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order' + params_row + '" class="form-control" />\n\
                        <input type="text" name="params[' + params_row + '][value]" value="" placeholder="Значення" id="input-value' + params_row + '" class="form-control" />\n\
                        <label class="control-label text-right" for="input-default' + params_row + '">Default</label>\n\
                        <input type="checkbox" name="params[' + params_row + '][default]" id="input-default' + params_row + '" class="form-control" /></td>\n\
                    <td class="text-right"><a href="" id="thumb-image' + params_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>\n\
                                    <input type="hidden" name="params[' + params_row + '][back_image]" value="" id="input-image' + params_row + '" /></td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#params-row' + params_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';	
	
	$('#params tbody').append(html);
	
	params_row++;
}
//--></script></div>
<?php echo $footer; ?> 