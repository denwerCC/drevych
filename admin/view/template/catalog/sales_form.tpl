<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-sales" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-sales" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) { ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="sales_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($sales_description[$language['language_id']]) ? $sales_description[$language['language_id']]['title'] : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="sales_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote"><?php echo isset($sales_description[$language['language_id']]) ? $sales_description[$language['language_id']]['description'] : ''; ?></textarea>
                                            <?php if (isset($error_description[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_description[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="sales_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($sales_description[$language['language_id']]) ? $sales_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="sales_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($sales_description[$language['language_id']]) ? $sales_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="sales_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($sales_description[$language['language_id']]) ? $sales_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="keyword" value="<?php echo $keyword; ?>" placeholder="<?php echo $entry_keyword; ?>" id="input-keyword" class="form-control" />
                                    <?php if ($error_keyword) { ?>
                                    <div class="text-danger"><?php echo $error_keyword; ?></div>
                                    <?php } ?>                
                                </div>
                            </div>
                            <div class="form-group text-left required">
                                <label class="col-sm-3 control-label" for="input-date-form">
                                    <span><?php echo $entry_date_from; ?></span>
                                </label>
                                <div class="col-sm-3">
                                    <?php if ($error_date_from) { ?>
                                    <div class="text-danger"><?php echo $error_date_from; ?></div>
                                    <?php } ?>
                                    <div class='input-group date' id='datetimepicker_from'>
                                        <input type='text' name="date_from" class="form-control" readonly="readonly" id="input-date-form" value="<?php echo $date_from; ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>

                                    </div>
                                </div>
                                <label class="col-sm-3 control-label" for="input-date-to">
                                    <span><?php echo $entry_date_to; ?></span>
                                </label>
                                <div class="col-sm-3">
                                    <?php if ($error_date_to) { ?>
                                    <div class="text-danger"><?php echo $error_date_to; ?></div>
                                    <?php } ?>
                                    <div class='input-group date' id='datetimepicker_to'>
                                        <input type='text' name="date_to" class="form-control" readonly="readonly" id="input-date-to" value="<?php echo $date_to; ?>"/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_category; ?></label>
                                <div class="col-sm-10">
                                    <select name="category_id" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($categories as $category) { ?>
                                        <?php if (isset($category_id) && $category_id == $category['category_id']) { ?>
                                        <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-product"></label>
                                <div class="col-sm-10">
                                    <input type="text" name="product" value="" placeholder="<?php echo $entry_products; ?>" id="input-product" class="form-control" />
                                    <div id="sale-product" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($sales_products as $product) { ?>
                                        <div id="sale-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
                                            <input type="hidden" name="sales_products[]" value="<?php echo $product['product_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="baner_category"><span><?php echo $entry_category_baner; ?></span></label>
                                <div class="col-sm-10">
                                    <a href="" id="thumb-image-category" data-toggle="image" class="img-thumbnail"><img src="<?php echo !empty($baner_category_thumb) ? $baner_category_thumb : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                    <input type="hidden" name="baner_category" value="<?php echo $baner_category ?>" id="input-image-category" />              
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="baner"><span><?php echo $entry_baner; ?></span></label>
                                <div class="col-sm-10">
                                    <a href="" id="thumb-image-baner" data-toggle="image" class="img-thumbnail"><img src="<?php echo !empty($baner_thumb) ? $baner_thumb : $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                    <input type="hidden" name="baner" value="<?php echo $baner ?>" id="input-image-baner" />              
                                </div>
                            </div>
                        </div>            
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--
  $('#language a:first').tab('show');
        //-->
        moment.locale('ru');
        $(function () {
            $('#datetimepicker_from').datetimepicker({
                Default: true,
                inline: true,
                sideBySide: true,
                format: "YYYY-MM-DD HH:mm",
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
            });
            $('#datetimepicker_to').datetimepicker({
                Default: true,
                inline: true,
                sideBySide: true,
                format: "YYYY-MM-DD HH:mm",
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
            });
        });
        $('input[name=\'product\']').autocomplete({
            'source': function (request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/sales/autocomplete&token=<?php echo $token; ?>&product_name=' + encodeURIComponent(request),
                    dataType: 'json',
                    success: function (json) {
                        response($.map(json, function (item) {
                            return {
                                label: item['name'],
                                value: item['product_id']
                            }
                        }));
                    }
                });
            },
            'select': function (item) {
                $('input[name=\'product\']').val('');

                $('#sale-product' + item['value']).remove();

                $('#sale-product').append('<div id="sale-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="sales_products[]" value="' + item['value'] + '" /></div>');
            }
        });

        $('#sale-product').delegate('.fa-minus-circle', 'click', function () {
            $(this).parent().remove();
        });
        </script>

    </div>
<?php echo $footer; ?>