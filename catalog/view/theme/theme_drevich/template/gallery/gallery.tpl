<?php echo $header; ?>
<div class="container-fluid">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
</div>
<div class="container-shadow gallery-page-inside">
    <div class="container-fluid">  
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
            <?php $class = 'col-xs-12 col-sm-12 col-sm-offset-0 col-md-12 col-lg-10 col-lg-offset-1'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">

                <div class="green-title"><?=$gallery_data['title']?></div>
                <div class=" gallery-inside">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 gallery-slider">
                            <ul class="bxslider">
                                <?php foreach($gallery_data['photos'] as $key => $value) { ?>
                                <li><img src="<?=$value ?>" alt="image" onclick="openModal();currentSlide(<?=$key ?> + 1)" class="hover-shadow cursor"></li>
                                <?php } ?>
                            </ul>
                            <div id="bx-pager" class="clearfix pager-gallery">
                                <?php foreach($gallery_data['photos'] as $key => $value) { ?>
                                <a data-slide-index="<?=$key ?>" href=""><img src="<?=$value ?>" alt="image"></a>
                                <?php } ?>
                            </div>
                        </div>
						
						<div id="myModal" class="modal">
						 
							<div class="modal-content  center-content">
								<div class="container-fluid">
									<div class="row">
									  
										<div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
											<span class="close cursor" onclick="closeModal()">X</span>
											<?php foreach($gallery_data['photos'] as $key => $value) { ?>
												 <div class="mySlides">
													<li><img src="<?=$value ?>" alt="image"></li>
												</div>
											<?php } ?>

											<a class="prev" onclick="plusSlides(-1)"><img src="/catalog/view/theme/theme_drevich/image/left.png" alt="left"></a>
											<a class="next" onclick="plusSlides(1)"><img src="/catalog/view/theme/theme_drevich/image/right.png" alt="right"></a>
											<div class="pager-gallery"> 
											<?php foreach($gallery_data['photos'] as $key => $value) { ?>
											
												<div class="column">
													<img src="<?=$value ?>" onclick="currentSlide(<?=$key ?>+1)" alt="Nature and sunrise" class="demo cursor">
												</div>
											
											 <?php } ?>
											 </div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						
						
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <!--<h3>fgfhfhfghfgfgfgh</h3>-->
                            <?= $gallery_data['description']; ?>
						</div>
                    </div>
                    <div class="row buttons">
                        <div class="col-xs-12 col-sm-6 col-md-5 small-phone">
                            <?php if (!empty($gallery_data['button_href'])){ ?>
                            <a href="<?= $gallery_data['button_href'] ?>" class="continue-href">&lt; <?=$look_product_href ?></a>
                            <?php } ?>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-2 small-phone">
                            <?php if(!empty($gallery_data['product_id'])){ $product_id = $gallery_data['product_id']; ?>
                            <button onclick="cart.add('<?= $product_id ?>')" class="btn  buy-product button_checkout"><?= $text_buy ?></button>
                            <?php } ?>
                        </div>
                        <div class="col-xs-6  col-sm-4 col-md-2 small-phone">
                            <?php if (!empty($gallery_data['price_text'])){ ?>
                            <h3 class="product-gallery-price"><?= $gallery_data['price_text'] ?></h3>
                            <?php } ?>
                        </div>
                        <div class="col-xs-8 col-sm-4 col-md-2 small-phone">
                            <div class="product-single-hardness-tree">
                                <h4 class="rating-title"><?=$text_rating ?></h4>
                                <div class="rating">
                                    <input type="radio" class="rating-input" value="5" 
                                           id="rating-input-1-5" name="rating-input-1" <? echo $gallery_data['rating'] == 5 ? 'checked="checked"' : '' ?>/>
                                    <label for="rating-input-1-5" class="rating-star"></label>
                                    <input type="radio" class="rating-input" value="4"
                                           id="rating-input-1-4" name="rating-input-1" <? echo $gallery_data['rating'] == 4 ? 'checked="checked"' : '' ?>/>
                                    <label for="rating-input-1-4" class="rating-star"></label>
                                    <input type="radio" class="rating-input" value="3"
                                           id="rating-input-1-3" name="rating-input-1" <? echo $gallery_data['rating'] == 3 ? 'checked="checked"' : '' ?>/>
                                    <label for="rating-input-1-3" class="rating-star"></label>
                                    <input type="radio" class="rating-input" value="2"
                                           id="rating-input-1-2" name="rating-input-1" <? echo $gallery_data['rating'] == 2 ? 'checked="checked"' : '' ?>/>
                                    <label for="rating-input-1-2" class="rating-star"></label>
                                    <input type="radio" class="rating-input" value="1"
                                           id="rating-input-1-1" name="rating-input-1" <? echo $gallery_data['rating'] == 1 ? 'checked="checked"' : '' ?>/>
                                    <label for="rating-input-1-1" class="rating-star"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-1 small-phone">
                            <div class="additional-options-cart">
                                <button type="button" data-toggle="tooltip" class="share btn-options"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="Share"></button>
                                <div class="social">
                                    <ul>
                                        <li class="facebook"><a target="_blank" href="<?=$gallery_data['fb_share'] ?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                        <li class="gplus"><a target="_blank" href="<?=$gallery_data['gplus_share'] ?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                        <li class="vk"><a target="_blank" href="<?=$gallery_data['vk_share'] ?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<script>
    function openModal() {
		
	  document.getElementById('myModal').style.display = "block";
	}

	function closeModal() {
	  document.getElementById('myModal').style.display = "none";
	}

	var slideIndex = 1;
	showSlides(slideIndex);

	function plusSlides(n) {
	  showSlides(slideIndex += n);
	}

	function currentSlide(n) {
	  showSlides(slideIndex = n);
	}

	function showSlides(n) {
	  var i;
	  var slides = document.getElementsByClassName("mySlides");
	  var dots = document.getElementsByClassName("demo");
	  if (n > slides.length) {slideIndex = 1}
	  if (n < 1) {slideIndex = slides.length}
	  for (i = 0; i < slides.length; i++) {
		  slides[i].style.display = "none";
	  }
	  for (i = 0; i < dots.length; i++) {
		  dots[i].className = dots[i].className.replace(" active", "");
	  }
	  slides[slideIndex-1].style.display = "block";
	  dots[slideIndex-1].className += " active";
	  
	}
</script>




<script>
    $('.rating input').on('click', function () {
        value = $(this).val();
        $.ajax({
            url: 'index.php?route=gallery/gallery/rating',
            type: 'post',
            dataType: 'json',
            data: {
                value: value,
                gallery_id: <?php echo $gallery_data['gallery_id']; ?> ,
            },
            success: function (json) {
                if (json['error']) {
                    location.reload();
                }
                if (json['success']) {
                    $('.rating-input').removeAttr('checked').attr('checked', false).filter('[value="' + json.value + '"]').attr('checked', true).trigger('click');
                }
            },
        });
        console.log($(this).val());
    });
</script>
<?php echo $footer; ?>
