<?php echo $header; ?>
<div class="container-fluid">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
</div>
<div class="container-fluid">
    <!--class="<//?php echo $class;?>"-->
    <div class="row">
        <div id="content" >
            <section class="gallery-page">
                <div class="container-fluid gallery-shadows">
                    <div class="col-md-11">
                        <div class="categories-grid">
                            <nav class="categories">
                                <ul class="categories sale_filter">
                                    <?php foreach($categoies_array as $category){ ?>
                                    <li><a href="<?=$category['href']?>" class="<?=$category['active']?> <?=$category['css_class']?>"><?=$category['text']?></a></li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div> 
                    </div>
                </div>
                <div class="container-shadow">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                            <div class="row">
                                <?php if(!empty($gallery_list)){ 
                                foreach ($gallery_list as $gallery){ ?>
                                <div class="col-xs-12 col-sm-6 col-md-6 gallery-block">
                                    <a href="<?=$gallery['gallery_href'] ?>">
                                        <figure class="gallery-item">
                                            <img src="<?=$gallery['banner'] ?>" alt="icon_alb">
                                            <figcaption class="gallery-figcaption center-content">
                                                <div class="gallery-content">
                                                    <h2><?=$gallery['short_description'] ?></h2>
                                                    <a href="<?=$gallery['gallery_href'] ?>" class="green-btn <?=$gallery['css_class'] ?>"><?=$text_more_inform ?></a>
                                                </div>
                                            </figcaption>			
                                        </figure>
                                    </a>
                                </div>
                                <?php } } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php echo $footer; ?>