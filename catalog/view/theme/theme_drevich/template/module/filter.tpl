<div class="panel panel-default hidden-xs filter-panel-box">
    <!-- <div class="panel-heading"><?php echo $heading_title; ?></div> -->
    <div class="list-group">
        <?php foreach ($filter_groups as $filter_group) { ?>
        <div class="single-filter">
            <a class="list-group-item"><?php echo $filter_group['name']; ?></a>
            <div class="list-group-item">
                <?php if ($filter_group['custom_class'] == 'dropdownlist') { ?>
                <div class="form-group">
                    <select class="selectpicker" name="filter[]" class="form-control">
                        <option value=""><?php echo $text_select; ?></option>
                        <?php foreach ($filter_group['filter'] as $filter) { ?>
                        <option  data-content="" value="<?php echo $filter['filter_id']; ?>" <?php echo $filter['checked'] == 'checked' ? 'selected' : ''?>>
                            <?php echo $filter['name']; ?>
                        </option>
                        <?php } ?>
                        <p><?php echo $filter['name']; ?></p>
                    </select>
                </div>					
                <?php } else { ?>
                <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>" class="<?php echo $filter_group['custom_class']; ?>">
                    <?php foreach ($filter_group['filter'] as $filter) { ?>
                    <?php $back_color = !empty($filter['back_color']) ? 'style="background-color:'.$filter['back_color'].'";' : ''?>
                    <!--<div class="checkbox">-->
                    <?php if(empty($filter['back_image'])){ ?>
                    <label class="select-checkbox">
                        <?php if (!empty($filter['back_color'])) { ?>
                        <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" <?php echo $filter['checked']; ?>/>
                               <span <?=$back_color?>> </span>
                        <p></p>
                        <?php } else { ?>
                        <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" <?php echo $filter['checked']; ?>/>
                               <span <?=$back_color?>></span>
                        <p><?php echo $filter['name']; ?></p>
                        <?php } ?>
                    </label>	
                    <?php } elseif(!empty($filter['back_image'])){ ?>
                    <!--checkbox with img--->
                    <label class="select-checkbox">
                        <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" <?php echo $filter['checked']; ?>/>
                               <img src="<?=$filter['back_image']?>" alt="1" class="center-block">
                        <p><?php echo $filter['name']; ?></p>
                    </label>
                    <?php } ?>
                    <!-- </div>-->
                    <?php } ?>                
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="panel-footer clearfix">        
        <button type="button" id="button-filter-clear" class="btn btn-primary btn-view"><?php echo $button_filter_clear; ?></button>
        <button type="button" id="button-filter" class="btn btn-primary btn-view pull-right"><?php echo $button_filter; ?></button>
    </div>
    <button class="advanced-search"><?php echo $advanced_search; ?></button>
    <div class="line"></div>
</div>
<script type="text/javascript"><!--
$('#button-filter').on('click', function () {
        filter = [];
        paramsLink = '';

        $('input[name^=\'filter\']:checked, select[name^=\'filter\'] option:selected').each(function (element) {
            filter.push(this.value);
        });

        $('.params-box input[name^=\'params\']:checked, select[name^=\'params\'] option:selected').each(function (element) {
            paramsLink += '&params[' + this.dataset.key + ']=' + this.value;
        });

        location = '<?php echo $action; ?>&filter=' + filter.join(',') + paramsLink;

    });
    $('#button-filter-clear').on('click', function () {
        location = '<?php echo $action; ?>';
    });

//--></script>
