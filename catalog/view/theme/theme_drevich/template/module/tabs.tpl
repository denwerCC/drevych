<!-- <div id="tabs<?php echo $module; ?>" class="owl-tabs">
    <?php /* foreach ($banners as $banner) { ?>
    <div class="item text-center grayscale">
        <?php if ($banner['link']) { ?>
        <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
        <?php } else { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
        <?php } ?>
    </div>
    <?php } */ ?>
</div> -->



<section class="home_window_2">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                <?php $count = 1; ?>                    
                <?php foreach ($banners as $i => $banner) { ?>
                <?php 
                if(($i+1) == 9 || ($i+1) == 8 || ($i+1) == 6 || ($i+1) == 5){
                $random_class = 'small-tab'; 
                }
                elseif (($i+1) == 7) {
                $random_class = 'larg-tab-col-2';
                }
                elseif (($i+1) == 4 || ($i+1) == 1) {
                $random_class = 'larg-tab';
                }
                elseif (($i+1) == 3 || ($i+1) == 2) {
                $random_class = 'small-tab-col-1';
                }
                else{
                $random_class = 'small-tab';
                }                    
                ?>
                <?php if($count == 1 || $count == 5) { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tab">
                    <?php } ?>
                    <div class="tabs<?php echo $module; ?>">
                        <a href="<?php echo $banner['link']; ?>">
                            <div class="tab-hover <?php echo $random_class; ?>">
                                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                                <div class="overlay">
                                    <div class="title-tab"><?php echo $banner['title']; ?></div>  
                                </div>
                            </div>
                        </a>
                    </div>
                    <?php if($count == 4 || $count == 9) { ?>
                </div>
                <?php } ?>
                <?php $count++; } ?>
            </div>
        </div>
    </div>
</section>
