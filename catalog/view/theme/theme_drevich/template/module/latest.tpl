<!--<h3><?php echo $heading_title; ?></h3>-->
<div class="row">
    <?php foreach ($products as $product) { ?>
    <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="product-thumb transition product">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                <p><?php echo $product['description']; ?></p>
                <?php if ($product['rating']) { ?>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($product['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if ($product['price'] && !$product['calculate']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                    <?php echo $product['price']; ?>
                    <?php } else { ?>
                    <span class="price-new"><?php echo $product['special']; ?></span>
                    <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                    <?php if ($product['tax']) { ?>
                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                    <?php } ?>
                </p>
                <?php } ?>
            </div>
           <!-- <div class="button-group">

                <!--<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                <a type="button" class="buy-product" href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
            </div>-->
			
			<div class="clearfix button-block">
                    <!--<button type="button" class=" buy-product" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>-->
                    <a type="button" class="buy-product" href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i><span class=" hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
                    <div class="additional-options">
                        <button type="button" data-toggle="tooltip" class="share btn-options" title="<?php echo $button_wishlist; ?>"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <?php } ?>
</div>
