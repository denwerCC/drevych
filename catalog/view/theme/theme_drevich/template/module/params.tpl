<div class="panel panel-default hidden-xs filter-panel-box params-box">
    <!-- <div class="panel-heading"><?php echo $heading_title; ?></div> -->
    <div class="list-group">
        <?php foreach ($params_groups as $params_group) { ?>
        <div class="single-params">
            <a class="list-group-item"><?php echo $params_group['name']; ?></a>
            <div class="list-group-item">
                <?php if ($params_group['custom_class'] == 'dropdownlist') { ?>
                <div class="form-group">
                    <select class="selectpicker" name="params[]" class="form-control">
                        <option data-key="<?php echo $params_group['secret_key']; ?>"  value=""><?php echo $text_select; ?></option>
                        <?php foreach ($params_group['params'] as $params) { ?>
                        <option  data-key="<?php echo $params_group['secret_key']; ?>" value="<?php echo $params['value'] ?>" <?php echo $params['checked'] == 'checked' ? 'selected' : ''?>>
                                 <?php echo $params['name']; ?>
                        </option>
                        <?php } ?>
                        <p><?php echo $params['name']; ?></p>
                    </select>
            </div>					
            <?php } else { ?>
            <div id="params-group<?php echo $params_group['params_group_id']; ?>" class="<?php echo $params_group['custom_class']; ?>">
                <?php foreach ($params_group['params'] as $params) { ?>
                <?php $back_color = !empty($params['back_color']) ? 'style="background-color:'.$params['back_color'].'";' : ''?>
                <!--<div class="checkbox">-->
                <?php if(empty($params['back_image'])){ ?>
                <label class="select-checkbox">
                    <?php if (!empty($params['back_color'])) { ?>
                    <input type="checkbox" 
                           name="params[]"                               
                           data-key="<?php echo $params_group['secret_key']; ?>" 
                           value="<?php echo $params['value']; ?>"
                           <?php echo $params['checked']; ?>
                           />
                           <span <?=$back_color?>> </span>
                    <p></p>
                    <?php } else { ?>
                    <input type="checkbox" 
                           name="params[]" 
                           data-key="<?php echo $params_group['secret_key']; ?>" 
                           value="<?php echo $params['value']; ?>"
                           <?php echo $params['checked']; ?>
                           />
                           <span <?=$back_color?>></span>
                    <p><?php echo $params['name']; ?></p>
                    <?php } ?>
                </label>	
                <?php } elseif(!empty($params['back_image'])){ ?>
                <!--checkbox with img--->
                <label class="select-checkbox">
                    <input type="checkbox" 
                           name="params[]" 
                           data-key="<?php echo $params_group['secret_key']; ?>" 
                           value="<?php echo $params['value']; ?>"
                           <?php echo $params['checked']; ?>
                           />
                           <img src="<?=$params['back_image']?>" alt="1" class="center-block">
                    <p><?php echo $params['name']; ?></p>
                </label>
                <?php } ?>
                <!-- </div>-->
                <?php } ?>                
            </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
</div>
</div>
