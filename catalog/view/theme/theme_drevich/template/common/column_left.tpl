<?php if ($modules) { ?>
<aside id="column-left" class="col-sm-3">
    <button class="mobile-filter btn-green"><i class="fa fa-filter" aria-hidden="true"></i><?php echo $button_filter_mobile; ?></button>
    <?php foreach ($modules as $module) { ?>
    <?php echo $module; ?>
    <?php } ?>
</aside>
<?php } ?>
