<section class="banner">
	<img src="<?=$prefooter?>" alt="banner">
	<div class="info"><a href="<?=$prefooter_href?>"><?=$prefooter_text?></a></div>
</section>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-11 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-6 col-lg-offset-3">
                <?php if ($informations) { ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!--<h5><?php echo $text_information; ?></h5>-->
                    <ul class="list-unstyled footer-nav">
                        <!--<?php if (isset($informations[4])){ ?><li><a href="<?php echo $informations[4]['href']; ?>"><?php echo $informations[4]['title']; ?></a></li><?php } ?>-->                        
                        <li><a href="<?php echo $newsblog_href; ?>"><?php echo $newsblog_href_text; ?></a></li>
                        <li><a href="<?php echo $sale_href; ?>"><?php echo $sale_href_text; ?></a></li>						
                        <?php if (isset($informations[11])){ ?><li><a href="<?php echo $informations[11]['href']; ?>"><?php echo $informations[11]['title']; ?></a></li><?php } ?>                        
                        <?php if (isset($informations[9])){ ?><li><a href="<?php echo $informations[9]['href']; ?>"><?php echo $informations[9]['title']; ?></a></li><?php } ?>
                        <?php if (isset($informations[7])){ ?><li><a href="<?php echo $informations[7]['href']; ?>"><?php echo $informations[7]['title']; ?></a></li><?php } ?>                        
                    </ul>
                </div>
                <?php } ?>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <!--<h5><?php echo $text_service; ?></h5>-->
                    <ul class="list-unstyled footer-nav">
                        <li><p><?php echo $text_social; ?> </p>
                            <ul class="footer-social">
                                <li><a href="https://www.facebook.com/Drevych/"  target="_blank" ><img src="/catalog/view/theme/theme_drevich/image/facebook.png" alt="facebook"></a></li>
                                <li><a href="https://plus.google.com/103815343581551247522?hl=uk"><img src="/catalog/view/theme/theme_drevich/image/google_plus.png" alt="google_plus"></a></li>
                                <li><a href="https://www.instagram.com/drevych_ua/"><img src="/catalog/view/theme/theme_drevich/image/instagram.png" alt="instagram"></a></li>
                                <li><a href="https://www.youtube.com/channel/UC6iuxmSF8vSnLpmkXrFgqGw" target="_blank"><img src="/catalog/view/theme/theme_drevich/image/youtube.png" alt="youtube"></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        <?php if (isset($informations[5])){ ?><li><a href="<?php echo $informations[5]['href']; ?>"><?php echo $informations[5]['title']; ?></a></li><?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	
	<a href="#" class="scrollup btn-options"><img src="/catalog/view/theme/theme_drevich/image/up-arrow.png" alt="up-arrow"></a>
</footer>
<!--popup-->
<div class="fon_pop_up"></div>
<div class="pop_up" role="alert">
    <div class="popup-close"><i class="fa fa-times fa-lg" aria-hidden="true"></i></div>
    <form action="" method="post" class="mail-form">
        <legend><?=$text_enter_form?></legend>
        <input type="hidden" name="product_by_one_click">
        <input type="text" name="name" class="fill-form-input" placeholder="<?=$text_name_and_surname?> *" required>
        <input type="tel" name="phone" class="fill-form-input" placeholder="<?=$text_phone_number?> *" required>
        <input type="email" name="email" class="fill-form-input" placeholder="<?=$text_email?>">        
        <input type="hidden" name="wood_type" id="wood_type_input_by_click">
        <textarea name="message" cols="40" rows="4" placeholder="<?=$text_messege?>"></textarea>
        <input type="submit" class="fill-form-button product-single-buy-one-click" value="<?=$text_send?>">
    </form>
</div>
<script type='text/javascript'>
function setSeries(product_id, image_url) {		
$.ajax({
    type: "post",
    url: "<?=$set_image_url?>",
    data: {

        product_id: product_id,
        product_image_url: image_url
    },
    dataType: 'json',
    success: function (response) {		
        if (response.success) {
            location.reload();
            $(".type-window li img").each(function() {
                path = $(this).attr("src");
                if (path.indexOf(image_url) + 1) {
                    $(".type-window li img").removeClass('active');
                    $(this).addClass('active');
                }
            });				
        }				
    }
});	

}
</script>
<!-- Theme created by Welford Media for OpenCart 2.0 www.welfordmedia.co.uk -->
<!— BEGIN JIVOSITE CODE {literal} —>
<script type='text/javascript'>
(function(){ var widget_id = 'leoFLpvwWI';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!— {/literal} END JIVOSITE CODE —>
</body></html>