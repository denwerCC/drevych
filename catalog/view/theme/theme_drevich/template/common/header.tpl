<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta property="og:image" content="http://customer.bookforum.ua/wp-content/uploads/2017/05/Drevych-LOGO.jpg" />
        <title><?php echo $title; ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
        <link href="/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

        <link href="/catalog/view/javascript/bxslider/jquery.bxslider.css" rel="stylesheet" media="screen" />
        <script src="/catalog/view/javascript/bxslider/jquery.bxslider.js" type="text/javascript"></script>


        <script src="/catalog/view/javascript/jquery/zoom-img/zoomsl-3.0.min.js" type="text/javascript"></script>
        <link href="/catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
        <link href="/catalog/view/theme/theme_drevich/stylesheet/stylesheet.css" rel="stylesheet" type="text/css">
        <?php foreach ($styles as $style) { ?>
        <link href="/<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <script src="/catalog/view/javascript/common.js" type="text/javascript"></script>
        <script src="/catalog/view/javascript/js.js" type="text/javascript"></script>
        <?php foreach ($links as $link) { ?>
        <link href="/<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($scripts as $script) { ?>
        <script src="/<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>
<div style="display:none;">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5K766PX');</script>
<!-- End Google Tag Manager -->

<!-- Код Google для тегу ремаркетингу -->

<!-- BEGIN PLERDY CODE -->
<script type="text/javascript" defer>
    var _protocol = (("https:" == document.location.protocol) ? " https://drevych.ua" : " http://drevych.ua");
    var _site_hash_code = "4ad9277dd748b20031a16c8d3d8e4bb5";
    var _suid = 772;
    </script>
<script type="text/javascript" defer src="https://a.plerdy.com/public/js/click/main.js"></script>
<!-- END PLERDY CODE -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '419082408561808'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=419082408561808&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Код Бітрікс -->

<!-- Кінець коду Бітрікс -->

<!-- Код Google карт -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCXS3dRkey3GxiYoFULEuv_V6f3PJFIXg4&amp;amp;amp;sensor=false"></script>

<!-- Код пуш повідомлень sendpulse -->
<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/b4c39791758613ee2ef3826a35964a8e_1.js" async></script>

<!--------------------------------------------------
Теги ремаркетингу не можна пов’язувати з особистою інформацією або розміщувати на сторінках, пов’язаних із категоріями делікатного характеру. Докладніші відомості та вказівки з налаштування тегу див. за адресою http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 831884966;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/831884966/?guid=ON&amp;script=0"/>
</div>
</noscript>



</div>
    </head>
    <body class="<?php echo $class; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5K766PX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <!-- <nav id="top">
             <div class="container">
                 
             </div>
         </nav>-->
        <header>
            <div class="container-fluid">
                <div class="row row-eq-height">
                    <div class="col-xs-12 col-sm-6 col-sm-push-3 col-md-6 col-md-push-3">
                        <div class="row top_header">						
                            <!--<div id="top-links" class="nav text-center">-->
                            <ul class="list-inline">
                                <li class="dropdown-language-list"> <span class="hidden-xs hidden-sm hidden-md"><?=$text_phone_text?></span><img src="/catalog/view/theme/theme_drevich/image/phone.png" class="icon-header" alt="phone"><!--<a href="<//?php echo $contact; ?>"><img src="/catalog/view/theme/theme_drevich/image/phone.png" class="icon-header" alt="phone"></a>
                                    <ul class="phone-list">-->
                                    <ul class="phone-list">
                                        <?php foreach($telephone as $value){ ?>
                                        <li><?=$value?></li>
                                        <?php } ?>
                                    </ul>
                                </li>
                                <li><span><?php echo $language; ?></span>
                                <li class="link-compare"><a href="<?php echo $compare_href; ?>" title="<?php echo $button_compare; ?>"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="Compare"></a>
                                </li>
                                <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span><span class="shopping_cart_count"> <?php echo $text_items; ?></span><img src="/catalog/view/theme/theme_drevich/image/bag.png" class="icon-header" alt="bag"></a></li>
                                <?php if ($logged) { ?>
                                <li><a href="<?php echo $account; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span><i class="fa fa-user" aria-hidden="true"></i></a></li>
                                <?php } else { ?>
                                <li><a href="<?php echo $login; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_login; ?></span><img src="/catalog/view/theme/theme_drevich/image/people.png" class="icon-header" alt="people"></a></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <div class="row search_header">
                            <?php echo $search; ?>
                        </div>

                        <div class="row">
                            <div id="top-links" class="menu_header nav text-center">
                                <ul class="list-inline">
                                    <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a></li>
                                    <?php if (isset($informations[4])){ ?><li><a href="<?php echo $informations[4]['href']; ?>"><?php echo $informations[4]['title']; ?></a></li><?php } ?>
                                    <?php if (isset($informations[6])){ ?><li><a href="<?php echo $informations[6]['href']; ?>"><?php echo $informations[6]['title']; ?></a></li><?php } ?>
                                    <li><a href="<?php echo $gallery_href; ?>"><?php echo $gallery_href_text; ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-3 col-sm-pull-6 col-md-3 col-md-pull-6 logo_wood main-categories-nav">
                        <div class="button_menu_wood clearfix"><div class="title-nav"> <?php echo $text_title_nav_wood; ?></div>
                            <div class="button_menu_header_wood"><span><!--<?php //echo $text_menu; ?>--><span></div>
                        </div>

                        <?php if($left_categories){ ?>
                        <ul class="menu_wood categories-nav">
                            <?php foreach($left_categories as $main_category) { ?>
                            <li class="list-item"><a class="link-main-category" href="<?php echo $main_category['href']; ?>"><?php echo $main_category['name']; ?></a>
                                <?php if(!empty($main_category['children'])){ ?>
                                <ul>
                                    <?php foreach( $main_category['children'] as $children ){ ?>
                                    <li><a href="<?php echo $children['href']; ?>"><?php echo $children['name']; ?></a></li>
                                    <?php foreach ($children['children_children_data'] as $child) { ?>
                                    <li class="menu_wood_child"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                </ul>
                                <?php } ?>

                                <?php } ?>
                            </li>	
                        </ul>
                        <?php } ?>
                    </div>

                    <div class="col-xs-6 col-sm-3 col-md-3 logo_smart main-categories-nav">
                        <div class="button_menu_smart"><div class="title-nav"> <?php echo $text_title_nav_smart; ?></div>
                            
							<div class="button_menu_header_wood"><span><!--<?php// echo $text_menu; ?>--></a><span></div>
                        </div>
                        <?php if($right_categories){ ?>
                        <ul class="menu_smart categories-nav">
                            <?php foreach( $right_categories as $main_category ) { ?>
                            <li class="list-item"><a class="link-main-category" href="<?php echo $main_category['href']; ?>"><?php echo $main_category['name']; ?></a>
                                <?php if(!empty($main_category['children'])){ ?>
                                <ul>
                                    <?php foreach( $main_category['children'] as $children ){ ?>
                                    <li><a href="<?php echo $children['href']; ?>"><?php echo $children['name']; ?></a></li>
                                    <?php foreach ($children['children_children_data'] as $child) { ?>
                                    <li class="menu_smart_child"><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                                <?php } ?>
                        </ul>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>
        <?php if ($categories) { ?>

        <div class="container">
            <nav id="menu" class="navbar">
                <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <?php foreach ($categories as $category) { ?>
                        <?php if ($category['children']) { ?>
                        <li class="dropdown"><a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $category['name']; ?></a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                    <ul class="list-unstyled">
                                        <?php foreach ($children as $child) { ?>
                                        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                </div>
                                <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a> </div>
                        </li>
                        <?php } else { ?>
                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                        <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div>
        <?php } ?>
