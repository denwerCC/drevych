<?php echo $header; ?>
<script type="text/javascript" src="/catalog/view/javascript/calculation.js"></script>
<script type="text/css" src="/catalog/view/javascript/bootstrap/css/bootstrap-select.jcss"></script>
<script type="text/javascript" src="/catalog/view/javascript/bootstrap/js/bootstrap-select.js"></script>
<div class="container-fluid">
    <ul class="breadcrumb back-calculation">
        <li><a href="<?php echo $back_to_list_url['href']; ?>"class="continue-href">&lt;<?php echo $back_to_list; ?></a></li>
                
	</ul>	
</div>
<div class="container-fluid">	
	
    <!--<a href="<?php echo $back_to_list_url['href']; ?>" class="continue-href breadcrumb">&lt;  <?php echo $back_to_list; ?></a>-->
    <div class="clearfix"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <div class="row ">
                <!-------images-------->
                <? $type_class = (!empty($window_types)) ? 'col-xs-8 col-sm-5 col-sm-offset-2 col-md-2 col-md-offset-0 col-lg-3 col-lg-offset-0 ' : 'col-sm-4' ?>                
                <div class="<?=$type_class?> type-single-page">
                    <?php if ($thumb) { ?>
                    <ul class="thumbnails  product-single-img">
                        <li><a class="thumbnail" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" class="shadow-border" /></a></li>
                    </ul>
                    <?php if ($images) { ?>
                    <ul class="thumbnails hidden-lg hidden-md hidden-sm hidden-xs">
                        <?php foreach ($images as $image) { ?>
                        <li class="image-additional"><a class="thumbnail" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                    <?php } ?>
                </div>
                <? if(isset($window_types) && !empty($window_types)){ ?>
                <div class="col-xs-4 col-sm-5 col-md-1 window-types-list">
                    <?php foreach ($window_types as $type){ ?>
                    <?php $typeImage = $type['image']; ?>
                    <div class="window_types text-center" onclick='setSeries("<?=$product_id; ?>", "<?=$typeImage ?>");'>
                        <img src="<?php echo $server.'/image/'.$type['image']; ?>" class="center-block"/>
                        <p><?php echo $type['name']; ?></p> 
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-8">
                    <div class="row row-padding">
                        <div class="col-xs-8 col-sm-6 col-md-6 text-left">
                            <div class="product-info block-product-title">
                                <span class="product-title"><?php echo $heading_title; ?></span>
                                <span><?php echo $model; ?></span> 
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-6 col-md-6 text-right">
                            <div class="additional-options">
                                <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product_id; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                                <button type="button" data-toggle="tooltip" class="share btn-options"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                                <div class="social">
                                    <ul>
                                        <li class="facebook"><a target="_blank" href="<?=$fb_share?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                        <li class="gplus"><a target="_blank" href="<?=$gplus_share?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                        <li class="vk"><a target="_blank" href="<?=$vk_share?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>                 
                    </div>
                    <div class="row">
                        <!--------product price and type options---------------------->
                        <div class="col-sm-12">
                            <?php foreach($wood_types as $type) { ?>
                            <?php $wood_name = explode('_', $type['type'])[0];?>
                            <div class="col-xs-12 col-sm-3 col-md-3" id="col_<?=$type['type']?>">
                                <div class="product-single-characteristic-tree shadow-border">
                                    <div class="option-type-head product-single-title">
                                        <h3><?php echo $wood[$wood_name]; ?></h3>
                                    </div>
                                    <div class="block-wood-attribute">
                                        <h4 class="wood-attribute-name"><?php echo $hardness; ?></h4>                                    
                                        <div class="wood-attribute">
                                            <? $i=1; while($i <= 5){ ?>
                                            <?php echo $type['hardness'] >= $i ? "<span class='flag_on'>1</span>" : "<span class='flag_off'>0</span>";
                                            $i++;
                                            } ?>
                                        </div>
                                        <h4 class="wood-attribute-name"><?php echo $structure; ?></h4>
                                        <div class="wood-attribute">
                                            <? $i=1; while($i <= 5){ ?>
                                            <?php echo $type['structure'] >= $i ? "<span class='flag_on'>1</span>" : "<span class='flag_off'>0</span>";
                                            $i++; 
                                            } ?>
                                        </div>
                                        <h4 class="wood-attribute-name"><?php echo $prestige; ?></h4>
                                        <div class="wood-attribute">
                                            <? $i=1; while($i <= 5){ ?>
                                            <?php echo $type['prestige'] >= $i ? "<span class='flag_on'>1</span>" : "<span class='flag_off'>0</span>";
                                            $i++; 
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="option-type-head" >
                                        <h3 id="h_price_<?php echo $type['type']; ?>" class="product-single-price">
                                            <?php echo $type['def_price']; ?>
                                        </h3>
                                    </div>
                                    <input class="changed_option_price"
                                           name="option[wood]"
                                           type="hidden" 
                                           value="<?php echo $type['type']; ?>">
                                    <input class="changed_option_price"
                                           name="price"
                                           id="calc_price_<?php echo $type['type']; ?>"
                                           data-target-id="h_price_<?php echo $type['type']; ?>" 
                                           type="hidden" 
                                           value="<?php echo $type['def_price_value']; ?>">
                                    <div class="by-button"
                                         data-rowid="col_<?=$type['type']?>">
                                        <button type="button" 
                                                data-loading-text="<?php echo $text_loading; ?>"                                                
                                                class="btn btn-success btn-block button-calc-cart product-single-buy">
                                            <?php echo $button_cart; ?>
                                        </button>
                                    </div>
                                    <div class="one-click-button one-click-popup">
                                        <button type="button" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-warning btn-block btn-view"><?php echo $button_one_click; ?></button>
                                    </div> 
                                </div>								
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="product">
                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />    
                <div class="row ">
                    <?php if ($options) { 
                    $last_index = count($options) - 1;
                    ?>
                    <?php foreach ($options as $i => $option) { ?>
                    <?php if($option['secret_key'] == 'var_a_key') {
                    echo '<div class="col-xs-12 col-sm-4 col-md-4 choose-size-door"> <h3>'.$spase_size.':</h3>';                    
                    } elseif($option['secret_key'] != 'var_a_key' && $option['secret_key'] != 'var_b_key' && $option['secret_key'] != 'var_t_key' && $i == 3 ) {
                    echo "<div class='col-xs-12 col-sm-8 col-md-8 product-single-atributes'>";
                    } ?>
                    <?php if ($option['type'] == 'select') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>" <?php echo $option['visible'] ? 'style="display:none;"' : ""; ?>>
                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                        <select class="selectpicker" name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                            <option value=""><?php echo $text_select; ?></option>
                            <?php foreach ($option['product_option_value'] as $option_value) { ?><?php 
                            if(isset($option_value['image']) && !empty($option_value['image'])){ 
                                $optionImage = $option_value['image'];
                            }else{
                                $optionImage = '';
                            } ?>
                            <option  data-content="<?php echo $option_value['name']; ?> <img src='<?=$optionImage ?>'>"
                                     value="<?php echo $option_value['product_option_value_id']; ?>" <?php echo $option_value['selected'] ? 'selected' : ''?>>                            
                            </option>
                            <?php } ?>
                        </select>
                    </div>					
                    <?php } ?>
                    <?php if ($option['type'] == 'radio') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>"  <?php echo $option['visible'] ? 'style="display:none;"' : ""; ?>>
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="radio block-radio">
                                <label class="select-radio">
                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php echo $option_value['selected'] ? 'checked' : ''?>/>
                                    <img src="/catalog/view/theme/theme_drevich/image/12.png">
                                    <p><?php echo $option_value['name']; ?></p>
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <?php } ?>
                    <?php if ($option['type'] == 'checkbox') { ?>
                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>"  <?php echo $option['visible'] ? 'style="display:none;"' : ""; ?>>
                        <label class="control-label"><?php echo $option['name']; ?></label>
                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" <?php echo $option_value['selected'] ? 'checked' : ''?>/>
                                    <?php if ($option_value['image']) { ?>
                                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
                                    <?php } ?>
                                    <?php echo $option_value['name']; ?>
                                </label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php if ($option['type'] == 'text') { ?>
                    <div class="row block-enter-width"  <?php echo $option['visible'] ? 'style="display:none;"' : ""; ?>>
                        <?php if(explode('-',$option['value']) && ($option['secret_key'] == 'var_a_key' || $option['secret_key'] == 'var_b_key')){ $variants = explode('-',$option['value']); ?>
                        <div class="col-xs-9 col-md-5  col-md-push-7">
                            <?php foreach($variants as $oneVar){ $val = $oneVar; ?>
                            <div class="badge variant_value <?php echo $option['var_a'] == $val || $option['var_b'] == $val ? 'active' : ''?>" 
                                 data-variant-for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $oneVar; ?></div>
                            <?php } ?>
                        </div>                        
                        <div class="col-xs-3 col-md-1 col-md-push-1">
                            <p class="enter-width"><?php echo $text_or; ?></p>
                        </div>
                        <?php } ?>
                        <?php
                        $value = $option['value'];
                        if( $option['secret_key'] == 'var_a_key' ){
                            if(isset($option['var_a']) && !empty($option['var_a'])) {
                                $value = $option['var_a'];
                            } else {
                                $value = $variants[0];
                            }
                        }
                        if( $option['secret_key'] == 'var_b_key'){
                            if(isset($option['var_b']) && !empty($option['var_b'])) {
                                $value = $option['var_b'];
                            } else {
                                $value = $variants[0];
                            }
                        } ?>
						<div class="col-xs-9 col-md-6 col-md-push-6">
                                                       
                        </div>
                        <div class="form-group col-xs-12 col-md-6 col-md-pull-6<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                            <input type="text" 
                                   name="option[<?php echo $option['product_option_id']; ?>]" 
                                   value="<?php echo $value ?>" 
                                   placeholder="<?php echo $option['name']; ?>" 
                                   id="input-option<?php echo $option['product_option_id']; ?>"
                                   onkeyup="return onlyNumbers(this);"
                                   onchange="return onlyNumbers(this);"
                                   class="form-control" />
                        </div>
                    </div>
                    <?php } ?>	
                    <?php if($option['secret_key'] == 'var_t_key') {
                    echo "</div>"; 
                    }elseif($i == $last_index) {
                    echo "</div>";
                    } ?>
                    <?php } ?>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <div class="recalculate-button">
                            <!------recalculation button----->
                            <!--button type="button" 
                                    data-loading-text="<?php echo $text_loading; ?>"
                                    id="button-recalculate"
                                    class="btn btn-success btn-block product-single-buy">
                                <?php echo $button_recalculate; ?>
                            </button>-->
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1<?php echo $class; ?>">
                        <ul class="nav nav-tabs  tabs">
                            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                            <?php if ($attribute_groups) { ?>
                            <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                            <?php } ?>
                            <?php if ($review_status) { ?>
                            <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content content">
                            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
                            <?php if ($attribute_groups) { ?>
                            <div class="tab-pane" id="tab-specification">
                                <table class="table table-bordered">
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>
                                        <tr>
                                            <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><?php echo $attribute['name']; ?></td>
                                            <td><?php echo $attribute['text']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <?php } ?>
                                </table>
                            </div>
                            <?php } ?>
                            <?php if ($review_status) { ?>
                            <div class="tab-pane" id="tab-review">
                                <form class="form-horizontal account-form" id="form-review">
                                    <div id="review"></div>
                                    <h2><?php echo $text_write; ?></h2>
                                    <?php if ($review_guest) { ?>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                            <input type="text" name="name" value="<?php echo $customer_name; ?>" id="input-name" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                            <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                            <div class="help-block"><?php echo $text_note; ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label"><?php echo $entry_rating; ?></label>
                                            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                            <input type="radio" name="rating" value="1" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="2" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="3" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="4" />
                                            &nbsp;
                                            <input type="radio" name="rating" value="5" />
                                            &nbsp;<?php echo $entry_good; ?></div>
                                    </div>
                                    <?php echo $captcha; ?>
                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <?php echo $text_login; ?>
                                    <?php } ?>
                                </form>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php if ($products) { ?>
                <h3><?php echo $text_related; ?></h3>
                <div class="row">
                    <?php $i = 0; ?>
                    <?php foreach ($products as $product) { ?>
                    <?php if ($column_left && $column_right) { ?>
                    <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                    <?php } elseif ($column_left || $column_right) { ?>
                    <?php $class = 'col-lg-4 col-md-4 col-sm-4 col-xs-12'; ?>
                    <?php } else { ?>
                    <?php $class = 'col-lg-3 col-md-3 col-sm-4 col-xs-12'; ?>
                    <?php } ?>
                    <div class="<?php echo $class; ?>">
                        <div class="product-thumb transition product">
                            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                            <div class="caption">
                                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                <p><?php echo $product['description']; ?></p>
                                <?php if ($product['rating']) { ?>
                                <div class="rating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($product['rating'] < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php if ($product['price']) { ?>
                                <p class="price">
                                    <?php if (!$product['special']) { ?>
                                    <?php echo $product['price']; ?>
                                    <?php } else { ?>
                                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                    <?php if ($product['tax']) { ?>
                                    <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                    <?php } ?>
                                </p>
                                <?php } ?>
                            </div> 
                            <div class="proposed-product">
                                <button type="button" class="btn-green" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
                                <div class="additional-options">
                                    <!--<button type="button" data-toggle="tooltip" title="В закладки" onclick="wishlist.add('62');"><i class="fa fa-heart"></i></button>
                                    <button type="button" data-toggle="tooltip" title="В сравнение" onclick="compare.add('62');"><i class="fa fa-exchange"></i></button>
                                    -->
                                    <button type="button" data-toggle="tooltip" class="btn-options" title="" onclick="compare.add('62');" data-original-title="В сравнение"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                                    <button type="button" data-toggle="tooltip" class="share btn-options" data-original-title="" title=""><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                                    <div class="social">
                                        <ul>
                                            <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                            <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                            <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
                    <div class="clearfix visible-md visible-sm"></div>
                    <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
                    <div class="clearfix visible-md"></div>
                    <?php } elseif ($i % 4 == 0) { ?>
                    <div class="clearfix visible-md"></div>
                    <?php } ?>
                    <?php $i++; ?>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: '/index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: '/index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
			
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('.shopping_cart_count').html(json['total']);
                                
				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('/index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: '/index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('/index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: '/index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
  // Initiate with custom caret icon
  $('select.selectpicker').selectpicker();
});
//--></script>
<?php echo $footer; ?>
