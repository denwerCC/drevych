<?php echo $header; ?>
<div class="container-fluid">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
</div>
<div class="container-fluid">
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <!--<h2><?php echo $heading_title; ?></h2>-->
      <?php if ($thumb == "my turn off" || $description) { ?>
      <!--<div class="row">
        <?/*php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } */?>
      </div>-->
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($products) { ?>
      <!--p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
      <div class="row sort-form">
        <div class="hidden-xs hidden-sm hidden-md col-xs-8 col-md-4 ">
          <div class="btn-group hidden-xs">
            <!--<button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>-->
            <!--<button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>-->
          </div>
        </div>
        <div class="col-xs-4 col-md-3 text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
        <div class="col-xs-5 col-md-3 text-right">
          <select id="input-limit" class="form-control" onchange="location = this.value;">
            <?php foreach ($limits as $limits) { ?>
            <?php if ($limits['value'] == $limit) { ?>
            <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
		<div class="col-xs-3 col-md-2 text-right">
            <?php echo $currency; ?>
        </div>
      </div>
      <br />
      <div class="row">
        <?php $productClass = isset($smart_category) && $smart_category ? 'col-xs-12 col-sm-4 col-md-3 smart_category' : 'col-xs-12 col-sm-6 col-md-6' ;?>
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list <?=$productClass?>">
          <div class="product-thumb product">
            <div class=" product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
              <?php if(isset($product['calculate']) && $product['calculate']) { ?>
              <div class=" product-info">
                <?php if(isset($product['new_product']) && $product['new_product']) { ?><h4 class="new_product btn-green product-stickers"><?=$text_new_product?></h4><?php } ?>
                <?php if(isset($product['sale_product']) && $product['sale_product']) { ?><h4 class="sale_item btn-green product-stickers"><?=$text_sale_product?></h4><?php } ?>
                <p> <span class="title"><?=$p_name?></span><span class="values value-title-product"><?php echo $product['name']; ?></span></p>
                <p> <span class="title"><?=$p_serie?></span><span class="values"><?=$product['model']?></span></p>
                <?php
                    if(isset($product['attribute_colors']) && !empty($product['attribute_colors']['attribute'])) { ?>
                        <p><span class='title'><?=$product['attribute_colors']['name']?></span>
                            <?php foreach ($product['attribute_colors']['attribute'] as $attribute){ ?>
                                <span class="values"><?=$attribute['name'] ?></span>
                            <?php } ?>                    
                        </p>
                <?php } ?>
                <p>    
                    <span class="title type-tree"><?=$p_wood_t?></span>
                    <?php if(isset($product['wood_type']) && !empty($product['wood_type'])){ ?>
                        <ul class="type-tree values">
                            <?php foreach ($product['wood_type'] as $type){ ?>
                            <?php $wood_name = explode('_', $type['type'])[0];?>
                                    <li><span class="wood_name"><?php echo $wood[$wood_name]; ?></span><span class="price"><?php echo $type['def_price']; ?><span></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </p>
                <p>               
                <? if(isset($product['window_types']) && !empty($product['window_types'])){ ?>
                <span class="title type-window"><?=$p_window_t?></span>
                <ul class="type-window values">
                    <?php $product_id = $product['product_id']; ?>
                    <?php foreach ($product['window_types'] as $type => $image){ ?>
                    <li class="li_img" onclick='setSeries("<?=$product_id; ?>", "<?=$image ?>");'>
                        <img class="<?=$type == $product['active_window_type'] ? 'active' : ''?>" src="<?php echo $server.'/image/'.$image ?>" style="height:50px; width: 30px;"><?php echo $type; ?>
                    </li>                                      
                    <?php } ?>
                </ul>
                <?php } ?>
                <!--<ul class="type-window values">
                    <li><img src="/catalog/view/theme/theme_drevich/image/2.png" alt="1">01312</li>
                    <li><img src="/catalog/view/theme/theme_drevich/image/2.png" alt="1">01313</li>
                    <li><img src="/catalog/view/theme/theme_drevich/image/2.png" alt="1">01314</li>
                    <li><img src="/catalog/view/theme/theme_drevich/image/2.png" alt="1">01315</li>
                </ul>-->
                </p>
                <div class="clearfix button-block">
                    <!--<button type="button" class=" buy-product" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>-->
                    <a type="button" class="buy-product" href="<?php echo urldecode($product['href']); ?>"><i class="fa fa-shopping-cart"></i><span class=" hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
                    <div class="additional-options">
                        <button type="button" data-toggle="tooltip" class="share btn-options" title="<?php echo $button_wishlist; ?>"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
              </div>
              <?php }else{ ?>
              <div class=" product-info">
                  <?php if(isset($product['new_product']) && $product['new_product']) { ?><h4 class="new_product btn-green product-stickers"><?=$text_new_product?></h4><?php } ?>
                  <?php if(isset($product['sale_product']) && $product['sale_product']) { ?><h4 class="sale_item btn-green product-stickers"><?=$text_sale_product?></h4><?php } ?>
                  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                  <p class="product-description"><?php echo $product['description']; ?></p>
                  <?php if ($product['rating']) { ?>
                  <div class="rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } ?>
                      <?php } ?>
                  </div>
                  <?php } ?>
                  <?php if ($product['price']) { ?>
                  <p class="price">
                      <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                      <?php } else { ?>
                      <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                      <?php } ?>
                      <?php if ($product['tax']) { ?>
                      <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                      <?php } ?>
                  </p>
                  <?php } ?>
                  <div class="clearfix button-block">
                      <a type="button" class=" buy-product"type="button" class=" buy-product" href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i><span class=" hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
                      <div class="additional-options">
                          <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                      -->
                        <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                        <button type="button" data-toggle="tooltip" class="share btn-options"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                            </ul>
                        </div>					  
                        </div>
                  </div>
              </div>
              <?php } ?>              
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
	  
		<div class="row">
			<?php if ($thumb) { ?>
			<div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
			<?php } ?>
			<?php if ($description) { ?>
			<div class="col-sm-10 sale-bunner"><?php echo $description; ?></div>
			<?php } ?>
		</div>
		
		
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
