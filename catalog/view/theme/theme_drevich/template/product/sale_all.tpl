 <section class="sale-page">
            <div class="container-fluid sale-shadows">
                <div class="col-md-1">  <div class="breadcrumb">Акції/</div></div>
                <div class="col-md-11">
                <div class="categories-grid">
                    <nav class="categories">
                        <ul class="sale_filter">
                            <li><a href="" class="active" data-filter="*">Усі</a></li>
                            <li><a href="" data-filter=".door">Двері</a></li>
                            <li><a href="" data-filter=".stairs">Сходи</a></li>
                            <li><a href="" data-filter=".floor">Підлога</a></li>
                            <li><a href="" data-filter=".panels">Панелі</a></li>
                            <li><a href="" data-filter=".delivery">HoReCa</a></li>
                            <li><a href="" data-filter=".furniture">Меблі</a></li>
                            <li><a href="" data-filter=".toys">Іграшки</a></li>
                            <li><a href="" data-filter=".laser_cutting">Лазерна різка</a></li>
                            <li><a href="" data-filter=".design">Дизайн</a></li>
                        </ul>
                    </nav>
                </div> 
            </div>
            </div>
            <div class="container-shadow">

                 <div class="container">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                        <div class="row sale_container">
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                                <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut </p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                               <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>

                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4 sale-box">
                               <a href="sale-inside.html">
                                    <img src="image/sale_img.png" alt="image">
                                    <div class="sale-info">
                                        <div class="sale-timer"></div>
                                        <div class="sale-title">
                                            <span class="date-sale"> з 3 вересня до 1 жовтня</span>
                                            <p>Lorem ipsum dolor sit amet, 
                                                consectetur adipisicing elit, 
                                                sed do eiusmod tempor incididunt ut labore
                                                 et dolore magna aliqua. Ut</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>