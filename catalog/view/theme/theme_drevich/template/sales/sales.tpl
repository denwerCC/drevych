<?php echo $header; ?>
<div class="container-fluid">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
</div>
<div class="container-fluid">

<!--class="<//?php echo $class;?>"-->
    <div class="row">
        <div id="content" >
            <section class="sale-page">
                <div class="container-fluid sale-shadows">
                    <div class="col-md-11">
                        <div class="categories-grid">
                            <nav class="categories">
                                <ul class="sale_filter">
                                    <?php foreach($categoies_array as $category){ ?>
                                    <li><a href="<?=$category['href']?>" class="<?=$category['active']?>"><?=$category['text']?></a></li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div> 
                    </div>
                </div>
                <div class="container-shadow">
                    <div class="container">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10 col-lg-offset-1">
                            <div class="row sale_container">
                                <?php if(!empty($sales_list)){ ?>
                                <?php foreach($sales_list as $sale) { ?>
                                <div class="col-xs-12 col-sm-6 col-md-4 " style='content:'>
                                    <div class="sale-box clearfix">
										<p class="text_one_sale"><?=$text_one_sale?></p>
										<a href="<?=$sale['sale_href']?>">
											<img src="<?=$sale['baner_category']?>" alt="image">
											<div class="sale-info">
												<div class="sale-timer"><?=$sale['days_left']?></div>
												<div class="sale-title">
													<span class="date-sale"><?=$sale['from_to_dates']?></span>
													<?=$sale['description']?>
												</div>
											</div>
										</a>
									</div>
                                </div>
                                <?php } ?>
                                <?php } else { ?>
                                <h2 class="text-center"><?=$text_empty_sale?><h2> 
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php echo $footer; ?>