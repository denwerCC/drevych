<?php echo $header; ?>
<div class="container-fluid">
	<ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	</ul>
</div>
<div class="container-shadow">
<div class="container-fluid">  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-xs-12 col-sm-12 col-sm-offset-0 col-md-12 col-lg-10 col-lg-offset-1'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
      <?php if ($products) { ?>
      <!--p><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></p>-->
      <div class="row row-eq-height">
        <div class='col-xs-12 col-sm-6 col-md-5 text-center'>
			<div class="sale-inside-img">
				<img src="<?=$baner?>" alt="image">
				<div class="img-info">
					<h3><a href="<?=$category_href?>"><?=$category_text?></a></h3>
				</div> 
			</div>
        </div>
        <div class='col-xs-12 col-sm-6 col-md-6 col-md-offset-1 text-center timer-block'>
		      <div class="timer-center">
				  <p class="titloftimer"><?=$text_to_end_sale ?></p>
				  <div id="timer"></div>
			  </div>
        </div>
      </div>
      <br />
      <div class="row">
        <?php $productClass = isset($smart_category) && $smart_category ? 'col-xs-12 col-sm-4 col-md-3 smart_category' : 'col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-0 col-md-5 col-md-offset-1' ;?>
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list <?=$productClass?>">
          <div class="product-thumb product">
            <div class=" product-img"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div>
              <?php if(isset($product['calculate']) && $product['calculate']) { ?>
              <div class=" product-info">
                <p> <span class="title"><?=$p_name?></span><span class="values"><?php echo $product['name']; ?></span></p>
                <p> <span class="title"><?=$p_serie?></span><span class="values"><?=$product['model']?></span></p>
                <?php
                    if(isset($product['attribute_colors']) && !empty($product['attribute_colors']['attribute'])) { ?>
                        <p><span class='title'><?=$product['attribute_colors']['name']?></span>
                            <?php foreach ($product['attribute_colors']['attribute'] as $attribute){ ?>
                                <span class="values"><?=$attribute['name'] ?></span>
                            <?php } ?>                    
                        </p>
                <?php } ?>
                <p>    
                    <span class="title"><?=$p_wood_t?></span>
                    <?php if(isset($product['wood_type']) && !empty($product['wood_type'])){ ?>
                        <ul class="type-tree values">
                            <?php foreach ($product['wood_type'] as $type){ ?>
                            <?php $wood_name = explode('_', $type['type'])[0];?>
                                    <li>
                                        <span class="wood_name"><?php echo $wood[$wood_name]; ?></span><span class="price"><?php echo $type['def_price']; ?><span>
                                    </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </p>
                <p>               
                <? if(isset($product['window_types']) && !empty($product['window_types'])){ ?>
                <span class="title"><?=$p_window_t?></span>
                <ul class="type-window values">
                    <?php foreach ($product['window_types'] as $type => $image){ ?>
                    <?php $product_id = $product['product_id']; ?>
                    <li onclick='setSeries("<?=$product_id; ?>", "<?=$image ?>");'>
                        <img class="<?=$type == $product['active_window_type'] ? 'active' : ''?>" src="<?php echo $server.'image/'.$image ?>" style="height:50px; width: 30px;"><?php echo $type; ?>
                    </li>
                    <?php } ?>
                </ul>
                <?php } ?>
                </p>
                <div class="clearfix button-block">
                    <a type="button" class="buy-product" href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i><span class=" hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
                    <div class="additional-options">
                        <button type="button" data-toggle="tooltip" class="share btn-options" title="<?php echo $button_wishlist; ?>"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
              </div>
              <?php }else{ ?>
              <div class=" product-info">
                  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                  <p><?php echo $product['description']; ?></p>
                  <?php if ($product['rating']) { ?>
                  <div class="rating">
                      <?php for ($i = 1; $i <= 5; $i++) { ?>
                      <?php if ($product['rating'] < $i) { ?>
                      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } else { ?>
                      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                      <?php } ?>
                      <?php } ?>
                  </div>
                  <?php } ?>
                  <?php if ($product['price']) { ?>
                  <p class="price">
                      <?php if (!$product['special']) { ?>
                      <?php echo $product['price']; ?>
                      <?php } else { ?>
                      <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                      <?php } ?>
                      <?php if ($product['tax']) { ?>
                      <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                      <?php } ?>
                  </p>
                  <?php } ?>
                  <div class="clearfix button-block">
                      <a type="button" class=" buy-product"type="button" class=" buy-product" href="<?php echo $product['href']; ?>"><i class="fa fa-shopping-cart"></i><span class="hidden-sm hidden-md"><?php echo $button_cart; ?></span></a>
                      <div class="additional-options">
                          <!--<button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
                          <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                      -->
                        <button type="button" data-toggle="tooltip" class="btn-options" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><img src="/catalog/view/theme/theme_drevich/image/justice.png" alt="justice"></button>
                        <button type="button" data-toggle="tooltip" class="share btn-options"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook"><a target="_blank" href="<?=$product['fb_share']?>"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></a></li>
                                <li class="gplus"><a target="_blank" href="<?=$product['gplus_share']?>"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></a></li>
                                <li class="vk"><a target="_blank" href="<?=$product['vk_share']?>"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></a></li>
                            </ul>
                        </div>					  
                        </div>
                  </div>
              </div>
              <?php } ?>              
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<script>
    // timeend= new Date(ГОД, МЕСЯЦ-1, ДЕНЬ, ЧАСЫ-1, МИНУТЫ);
    timeend= new Date(<?=(int)date('Y', strtotime($date_to))?>, <?=(int)date('n', strtotime($date_to))?>-1, <?=(int)date('j', strtotime($date_to))?>, <?=(int)date('G', strtotime($date_to))?>, <?=(int)date('i', strtotime($date_to))?>);
    function time() {
        today = new Date();
        today = Math.floor((timeend-today)/1000);
        tsec=today%60; today=Math.floor(today/60); if(tsec<10)tsec='0'+tsec;
        tmin=today%60; today=Math.floor(today/60); if(tmin<10)tmin='0'+tmin;
        thour=today%24; today=Math.floor(today/24);
        if (parseInt(today) <= 0 && parseInt(thour) <= 0 && parseInt(tmin) <= 0 && parseInt(tsec) <= 0){
            document.getElementById('timer').innerHTML;
            window.location = '/';
        } else {
            timestr= "<span>"+today +" <b><?=$text_days?></b> </span><span>"+ thour+" <b><?=$text_hours?></b> </span><span>"+tmin+" <b><?=$text_minutes?></b> </span><span>"+tsec+" <b><?=$text_seconds?></b> </span>";
            document.getElementById('timer').innerHTML=timestr;
        }         
        window.setTimeout("time()",1000);
        
    }
    time();
</script>
</script>
<?php echo $footer; ?>
