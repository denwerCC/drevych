<?php echo $header; ?>
<div class="container-fluid">
    <div class="col-xs-12 col-md-1">
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>  
    <div class="col-xs-12 col-md-11">
        <div class="categories-grid">
            <nav class="categories">
                <ul class="sale_filter">
                    <?php foreach ($top_categories as $new_category){ ?>
                    <li><a href="<?php echo $new_category['href']; ?>" class="<?php echo $new_category['active']; ?>" data-filter="*"><?php echo $new_category['name']; ?></a></li>
                    <?php } ?>
                </ul>
            </nav>
        </div> 
    </div>
</div>  
<div class="container-shadow">
<div class="container">
  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h2><?php echo $heading_title; ?></h2>
      <?php if ($thumb || $description) { ?>
      <div class="row">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($articles) { ?>
      <div class="row">
	    <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
			<?php foreach ($articles as $article) { ?>
			<div class="product-layout product-list col-xs-6">
			  <div class="product-thumb news-thumb">
				<div class="image img-category-news"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
				<div class="caption caption-category-news">
					<h4><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
					<!--<p class="hidden-xs hidden-sm hidden-md"><//?php echo $article['preview']; ?></p>-->
					<? if ($article['attributes']) { ?>
						<h5><?=$text_attributes;?></h5>
						<? foreach ($article['attributes'] as $attribute_group) { ?>
							<? foreach ($attribute_group['attribute'] as $attribute_item) { ?>
							<b><?=$attribute_item['name'];?>:</b> <?=$attribute_item['text'];?><br />
							<? } ?>
						<? } ?>
					<? } ?>
				</div>
			  </div>
			</div>
			<?php } ?>
		
		</div>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$articles) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>