<html><meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <body>

        <table style="font-family: 'century-gothic', sans-serif; font-size: 14px;" >
			<tr>
				<td colspan="6" align="center"><a href="<?php echo $url_home; ?>" style=" color: #0c3e01; font-size: 18px; text-decoration: none; text-align: center; " >drevych.com.ua</a></td>
            </tr>           
		   <tr>
                <td rowspan="2"><img src="./catalog/view/theme/theme_drevich/image/logo-larg.png" class="img-thumbnail" alt="logo-wood" style="  width: 400px; "/></td>
                <!-- <td rowspan="2"><img src="/catalog/view/theme/theme_drevich/image/wood.png" alt="logo-wood"/></td>-->  
                <!-- <td rowspan="2"><img src="/catalog/view/theme/theme_drevich/image/logo-tree-black.png" alt="logo-smart"/></td>
                <td rowspan="2"><img src="/catalog/view/theme/theme_drevich/image/logo-smart.png" alt="logo-smart"/></td>-->
				 <td style="font-size: 25px; text-align: right; " >067-99-11-6-11</td>
            </tr>
			<tr>
				<td style="font-size: 25px; text-align: right; ">drevych1@gmail.com</td>
            </tr>
           <!-- <tr>
                <td align="center"><a href="</?php echo $url_home; ?>" style="color: #0c3e01;  text-decoration: none; margin0:0 6px;"></?php echo $text_home; ?></a></td>
                </?php if (isset($informations[4])){ ?><td align="center"><a href="</?php echo $informations[4]['href']; ?>" style="color: #0c3e01;  text-decoration: none; margin0:0 6px;"></?php echo $informations[4]['title']; ?></a></td></?php } ?>
                </?php if (isset($informations[6])){ ?><td colspan="4" align="center"><a href="</?php echo $informations[6]['href']; ?>" style="color: #0c3e01;  text-decoration: none; margin0:0 6px;"></?php echo $informations[6]['title']; ?></a></td></?php } ?>
                </?php if (isset($informations[7])){ ?><td align="center"><a href="</?php echo $informations[7]['href']; ?>" style="color: #0c3e01;  text-decoration: none; margin:0 6px;"></?php echo $informations[7]['title']; ?></a></td></?php } ?>
            </tr>-->
        </table>



        <div class="container">

            <?php if ($attention) { ?>
            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <?php } ?>
            <div class="row"><?php echo $column_left; ?>
                <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
                <?php } ?>
                <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="cart-form-submit">
                        <div class="table-responsive">
                            <table class="table table-bordered collapse" style="font-family: 'century-gothic', sans-serif;" align="center">
                                <thead class="title_checkout" style="border-bottom:gray thin solid;">
                                    <tr>
                                        <td class="text-center" colspan="2"><?php echo $column_image; ?></td>
                                        <td class="text-left" colspan="2"><?php echo $column_name; ?></td>
                                        <td class="text-left" colspan="2"><?php echo $column_model; ?></td>
                                        <td class="text-right" colspan="2"><?php echo $column_price; ?></td>
                                        <td class="text-left" colspan="2"><?php echo $column_quantity; ?></td>                                
                                        <td class="text-right" colspan="2"><?php echo $column_total; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($products as $product) { ?>
                                    <tr class="product_one_checkout " style="border-bottom:grey thin solid;">
                                        <td colspan="2"class="text-center text-middle"><?php if ($product['thumb']) { ?>
                                            <img src="./<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                            <?php } ?>
                                        </td>
                                        <td colspan="2" class="text-left name_one_product_checkout text-middle"><?php echo $product['name']; ?>
                                            <?php if (!$product['stock']) { ?>
                                            <span class="text-danger">***</span>
                                            <?php } ?>                                    
                                            <?php if ($product['reward']) { ?>
                                            <br />
                                            <small><?php echo $product['reward']; ?></small>
                                            <?php } ?>
                                            <?php if ($product['recurring']) { ?>
                                            <br />
                                            <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                                            <?php } ?>

                                        </td>
                                        <td colspan="2" class="text-left model_one_product_checkout text-middle">
                                            <?php echo $product['model']; ?><?php if ($product['option']) { ?>
                                            <?php foreach ($product['option'] as $option) { ?>
                                            <br />
                                            <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                            <?php } ?>
                                            <?php } ?>                                
                                        </td>
                                        <td colspan="2" class="text-right price_one_product_checkout text-middle"><?php echo $product['price']; ?></td>
                                        <td colspan="2" class="text-left text-middle">
                                            <div class="input-group btn-block count_one_product_checkout" style="max-width: 200px;">
                                                <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control count_input" />
                                                <span class="input-group-btn">
                                                    <button type="submit" data-toggle="tooltip" title="+" class="btn plus_one_product" onclick="plus(this);"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                    <button type="submit" data-toggle="tooltip" title="-" class="btn minus_one_product" onclick="minus(this);"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger delete_one_product" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times" aria-hidden="true"></i></button>
                                                </span>
                                            </div>
                                        </td>                                
                                        <td colspan="2" class="text-right total_price_product_checkout text-middle"><?php echo $product['total']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                    <br />
                    <div class="row">
                        <div class="col-sm-12" style="font-family: 'century-gothic', sans-serif; text-align: right; margin-left: 66.66666667%;border-top:gray thin solid;">
                            <table class="table table-bordered" align="center">
                                <?php foreach ($totals as $total) { ?>
                                <tr  >
                                    <td class="text-right total_price"><?php echo $total['title']; ?>:</td>
                                    <td class="text-right total_price"><?php echo $total['text']; ?></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    <?php echo $content_bottom; ?></div>
                <?php echo $column_right; ?></div>
        </div>
    </body>
</html>