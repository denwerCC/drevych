<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <?php if ($attention) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $attention; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="cart-form-submit">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="title_checkout">
                            <tr>
                                <td class="text-center"><?php echo $column_image; ?></td>
                                <td class="text-left"><?php echo $column_name; ?></td>
                                <td class="text-left"><?php echo $column_model; ?></td>
                                <td class="text-right"><?php echo $column_price; ?></td>
                                <td class="text-left"><?php echo $column_quantity; ?></td>                                
                                <td class="text-right"><?php echo $column_total; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) { ?>
                            <tr class="product_one_checkout ">
                                <td class="text-center text-middle"><?php if ($product['thumb']) { ?>
                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
                                    <?php } ?></td>
                                <td class="text-left name_one_product_checkout text-middle"><?php echo $product['name']; ?>
                                    <?php if (!$product['stock']) { ?>
                                    <span class="text-danger">***</span>
                                    <?php } ?>                                    
                                    <?php if ($product['reward']) { ?>
                                    <br />
                                    <small><?php echo $product['reward']; ?></small>
                                    <?php } ?>
                                    <?php if ($product['recurring']) { ?>
                                    <br />
                                    <span class="label label-info"><?php echo $text_recurring_item; ?></span> <small><?php echo $product['recurring']; ?></small>
                                    <?php } ?></td>
                                <td class="text-left model_one_product_checkout text-middle">
                                    <?php echo $product['model']; ?><?php if ($product['option']) { ?>
                                    <?php foreach ($product['option'] as $option) { ?>
                                    <br />
                                    <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
                                    <?php } ?>
                                    <?php } ?>                                
                                </td>
                                <td class="text-right price_one_product_checkout text-middle"><?php echo $product['price']; ?></td>
                                <td class="text-left text-middle">
                                    <div class="input-group btn-block count_one_product_checkout" style="max-width: 200px;">
                                        <input type="text" name="quantity[<?php echo $product['cart_id']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" class="form-control count_input" />
                                        <span class="input-group-btn">
                                            <button type="submit" data-toggle="tooltip" title="+" class="btn plus_one_product" onclick="plus(this);"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                            <button type="submit" data-toggle="tooltip" title="-" class="btn minus_one_product" onclick="minus(this);"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                            <button type="button" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger delete_one_product" onclick="cart.remove('<?php echo $product['cart_id']; ?>');"><i class="fa fa-times" aria-hidden="true"></i></button>
                                        </span>
                                    </div>
                                </td>                                
                                <td class="text-right total_price_product_checkout text-middle"><?php echo $product['total']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
            <br />
            <div class="row">
                <div class="col-sm-4 col-sm-offset-8">
                    <table class="table table-bordered">
                        <?php isset($totals[1]) ? $totals = [0 => $totals[1]] : $totals = $totals;?>
                        <?php foreach ($totals as $total) { ?>
                        <tr>
                            <td class="text-right total_price"><?php echo $total['title']; ?>:</td>
                            <td class="text-right total_price"><?php echo $total['text']; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            <div class="buttons">
                <a href="<?php echo $continue; ?>" class="continue-href"><  <?php echo $button_shopping; ?></a>
                <div class="pull-right checkout-button-options clearfix">
                    <div class="additional-options-cart">
                        <a type="button" class="btn-options download" data-toggle="tooltip" title="" target="_blank" data-original-title="download" href="<?=$download_link?>"><img src="/catalog/view/theme/theme_drevich/image/download.png" class="center-block" alt="download"></a>
                        <button type="button" class="share btn-options" data-toggle="tooltip" title=""  data-original-title="share"><img src="/catalog/view/theme/theme_drevich/image/share.png" alt="share"></button>
                        <div class="social">
                            <ul>
                                <li class="facebook" onclick="sharePdf('facebook');"><img src="/catalog/view/theme/theme_drevich/image/facebook-logo.png" alt="facebook"></li>
                                <li class="gplus" onclick="sharePdf('gplus');"><img src="/catalog/view/theme/theme_drevich/image/google-plus-logo.png" alt="google_plus"></li>
                                <li class="vk" onclick="sharePdf('vk');"><img src="/catalog/view/theme/theme_drevich/image/vk-logo.png" alt="vk"></li>
                            </ul>
                        </div>
                    </div>
                    <a href="<?php echo $checkout; ?>" class="btn  buy-product button_checkout"><?php echo $button_checkout; ?></a>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script>
    function plus(element) {
        $val = $(element).parent().prev('input').val();
        $val = parseInt($val) + 1;
        if (!$val < 1) {
            $(element).parent().prev('input').val($val);
        }
    }
    function minus(element) {
        $val = $(element).parent().prev('input').val();
        $val = parseInt($val) - 1;
        if (!$val < 1) {
            $(element).parent().prev('input').val($val);
        }
    }
    function sharePdf(shareType) {
        $.ajax({
            type: "post",
            data: {
                shareType: shareType,
                sharePdf: 'true'
            },
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    setTimeout(function () {
                        window.location.href = response.url;
                    }, 1500);
                } else {
                    alert("Error");
                }
            },
            error: function (jqXHR, exception) {
                alert("Error" + jqXHR + exception);
            }
        });
    }
</script>