<?php if ($heading_title) { ?>
<h3 class="hidden-xs hidden-sm hidden-md hidden-lg"><?php echo $heading_title; ?></h3>
<?php } ?>
<?php if ($html) { ?>
<?php echo $html; ?>
<?php } ?>
<div class="row">
	<div class="col-xs-12 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 container-news-block">
	  <?php foreach ($articles as $article) { ?>
	  <div class="product-layout col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="product-thumb transition">
		  <?php if ($article['thumb']) { ?>
		  <div class="image hidden-xs hidden-sm hidden-md hidden-lg"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['title']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
		  <?php } ?>
		  
		  <div class="caption">
			<h4 class=""><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
		    <p class="news">	
				<?php echo $article['preview']; ?>
			</p>
		  </div>
		  
		  <div class="button-group read-next">
			<button   onclick="location.href = ('<?php echo $article['href']; ?>');" data-toggle="tooltip" title="<?php echo $text_more; ?>"><i class="fa fa-share hidden-xs hidden-sm hidden-md hidden-lg"></i> <span><?php echo $text_more; ?></span></button>
			<?php if ($article['date']) { ?><button class="hidden-xs hidden-sm hidden-md hidden-lg" type="button" data-toggle="tooltip" title="<?php echo $article['date']; ?>"><i class="fa fa-clock-o hidden-xs hidden-sm hidden-md hidden-lg"></i></button><?php } ?>
			<button class="hidden-xs hidden-sm hidden-md hidden-lg" type="button" data-toggle="tooltip" title="<?php echo $article['viewed']; ?>"><i class="fa fa-eye hidden-xs hidden-sm hidden-md hidden-lg"></i></button>
		  </div>
		</div>
	  </div>
	  <?php } ?>
	</div>
</div>
<?php if ($link_to_category) { ?>
<a class="hidden-xs hidden-sm hidden-md hidden-lg" href="<?php echo $link_to_category; ?>"><?php echo $text_more; ?> <?php echo $heading_title; ?></a>
<?php } ?>