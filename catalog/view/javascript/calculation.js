/*
 * Calculation js
 * author Vasyl
 * only for calculation.tpl
 */

function onlyNumbers(input) {
    input.value = input.value.replace(/[^\d,]/, '');
    input.maxLength = 6;
}
;

$(document).ready(function () {
    $('.changed_option_price').on('change', function (e) {
        hId = $(this).data('target-id');
        $('#' + hId).text($(this).val());
    });

    /*
     * js for variants
     */
    $('.variant_value').on('click', function (e) {
        elementId = $(this).data('variant-for');
        $('#' + elementId).val($(this).text()).trigger('change');
    });

    /*
     * calculation ajax
     */
    $('#product select, #product input, #product textarea').on('change keyup load', function () {
        $.ajax({
            url: '/index.php?route=product/product/calculate_conf_price',
            type: 'post',
            data: $('#product input[type=\'text\'], \n\
                    #product input[type=\'hidden\'], \n\
                    #product input[type=\'radio\']:checked, \n\
                    #product input[type=\'checkbox\']:checked, \n\
                    #product select, \n\
                    #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                //$('#button-recalculate').button('loading');
            },
            complete: function () {
                //$('#button-recalculate').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    if (json['error']['message']) {
                        alert(json['error']['message']);
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }
                if (!json['error'] && json['success']) {
                    for (x in json['success']) {
                        $('#calc_price_' + x).val(json['success'][x]).change();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    $('.by-button').on('click', function () {
        col = '#' + $(this).data('rowid');
        $.ajax({
            url: '/index.php?route=checkout/cart/add',
            type: 'post',
            data: $(col + ' input[type=\'hidden\'] ,\n\
                    #product input[type=\'text\'], \n\
                    #product input[type=\'hidden\'],\n\
                    #product input[type=\'radio\']:checked, \n\
                    #product input[type=\'checkbox\']:checked, \n\
                    #product select, \n\
                    #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {

                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('.shopping_cart_count').html(' ' + json['total']);

                    $('html, body').animate({scrollTop: 0}, 'slow');

                    $('#cart > ul').load('/index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
});