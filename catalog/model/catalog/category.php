<?php

class ModelCatalogCategory extends Model {

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int) $category_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row;
    }

    public function getCategories($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getCategoriesWithSide($parent_id = 0, $side = '') {

        if ($side == 'left') {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' AND c.mleft = '1' ORDER BY c.sort_order, LCASE(cd.name)");
        }

        if ($side == 'right') {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' AND c.mright = '1' ORDER BY c.sort_order, LCASE(cd.name)");
        }

        return $query->rows;
    }

    public function getCategoryFilters($category_id) {
        $implode = array();

        $query = $this->db->query("SELECT filter_id FROM " . DB_PREFIX . "category_filter WHERE category_id = '" . (int) $category_id . "'");

        foreach ($query->rows as $result) {
            $implode[] = (int) $result['filter_id'];
        }

        $filter_group_data = array();

        if ($implode) {
            $filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order, fg.custom_class FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");

            foreach ($filter_group_query->rows as $filter_group) {
                $filter_data = array();

                $filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name, f.back_color, f.back_image  FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int) $filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

                foreach ($filter_query->rows as $filter) {
                    $filter_data[] = array(
                        'filter_id' => $filter['filter_id'],
                        'name' => $filter['name'],
                        'back_color' => $filter['back_color'],
                        'back_image' => $filter['back_image']
                    );
                }

                if ($filter_data) {
                    $filter_group_data[] = array(
                        'custom_class' => $filter_group['custom_class'],
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'name' => $filter_group['name'],
                        'filter' => $filter_data
                    );
                }
            }
        }

        return $filter_group_data;
    }

    public function getCategoryLayoutId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category_to_layout WHERE category_id = '" . (int) $category_id . "' AND store_id = '" . (int) $this->config->get('config_store_id') . "'");

        if ($query->num_rows) {
            return $query->row['layout_id'];
        } else {
            return 0;
        }
    }

    public function getTotalCategoriesByCategoryId($parent_id = 0) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = '" . (int) $parent_id . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "' AND c.status = '1'");

        return $query->row['total'];
    }

    public function getCategoriesWithoutParent() {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.parent_id = 0 AND cd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int) $this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");

        return $query->rows;
    }

    public function getParams() {
        $params_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "params_filter_group fg LEFT JOIN " . DB_PREFIX . "params_filter_group_description fgd ON (fg.params_group_id = fgd.params_group_id) WHERE fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND fg.visible = 1 ORDER BY fg.sort_order, LCASE(fgd.name)");

        $params_group_data = array();
        //var_dump($params_group_query->rows);die;
        foreach ($params_group_query->rows as $params_group) {

            $params_data = array();

            $params_query = $this->db->query("SELECT DISTINCT *  FROM " . DB_PREFIX . "params_filter f LEFT JOIN " . DB_PREFIX . "params_filter_description fd ON (f.params_id = fd.params_id) WHERE f.params_group_id = '" . (int) $params_group['params_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");

            foreach ($params_query->rows as $params) {
                $params_data[] = array(
                    'params_id' => $params['params_group_id'],
                    'name' => $params['name'],
                    'back_color' => $params['back_color'],
                    'back_image' => $params['back_image'],
                    'value' => $params['value'],
                    'default' => $params['default']
                );
            }
            if ($params_data) {
                $params_group_data[] = array(
                    'custom_class' => $params_group['custom_class'],
                    'params_group_id' => $params_group['params_group_id'],
                    'secret_key' => $params_group['secret_key'],
                    'name' => $params_group['name'],
                    'params' => $params_data
                );
            }
        }
        return $params_group_data;
    }

    public function getDefaultParams() {
        $params_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "params_filter_group fg LEFT JOIN " . DB_PREFIX . "params_filter_group_description fgd ON (fg.params_group_id = fgd.params_group_id) WHERE fgd.language_id = '" . (int) $this->config->get('config_language_id') . "' ORDER BY fg.sort_order, LCASE(fgd.name)");

        $params_data = array();
        foreach ($params_group_query->rows as $params_group) {            

            $params_query = $this->db->query("SELECT DISTINCT *  FROM " . DB_PREFIX . "params_filter f LEFT JOIN " . DB_PREFIX . "params_filter_description fd ON (f.params_id = fd.params_id) WHERE f.params_group_id = '" . (int) $params_group['params_group_id'] . "' AND fd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND f.default = 1 ORDER BY f.sort_order, LCASE(fd.name)");
            
            foreach ($params_query->rows as $params) {
                $params_data[$params_group['secret_key']] = $params['value'];
            }
        }
        return $params_data;
    }

}
