<?php

class ModelCatalogSales extends Model {

    public function getSale($sale_id) {
        $query = $this->db->query("SELECT DISTINCT * "
                . " FROM `" . DB_PREFIX . "sales` `main` "
                . " LEFT JOIN `" . DB_PREFIX . "sales_description` `desc` ON (`main`.`id` = `desc`.`sale_id`) "
                . " LEFT JOIN `" . DB_PREFIX . "sales_products` `prod` ON (`main`.`id` = `prod`.`sale_id`) "
                . " WHERE `main`.`id` = '" . (int) $sale_id . "'"
                . " AND `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");

        $sale_data = ['sale' => $query->row];
        global $loader, $registry;
        $loader->model('catalog/product');
        $model = $registry->get('model_catalog_product');
        foreach ($query->rows as $result) {
            $sale_data['products'][$result['product_id']] = $model->getProduct($result['product_id']);
        }
        return $sale_data;
    }

    public function getSales($category_id = 0) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "sales`AS `main` "
                . "LEFT JOIN `" . DB_PREFIX . "sales_description` `desc` ON (`main`.`id` = `desc`.`sale_id`) "
                . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "' ";
        $sql .= $category_id > 0 ? " AND `main`.`category_id` = '" . $category_id . "'" : "";
        $sql .= "AND (`main`.`date_from` <  Now() AND `main`.`date_to` >= Now())";
        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategories() {
        $sql = $this->db->query("SELECT DISTINCT `main`.`category_id`, `desc`.`name` FROM `" . DB_PREFIX . "sales` AS `main` "
                . "LEFT JOIN `" . DB_PREFIX . "category_description` `desc` ON (`main`.`category_id` = `desc`.`category_id`) "
                . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");
        return $sql->rows;
    }
    
    public function getIdSalesProducts($category_id) {
        $sql = $this->db->query("SELECT DISTINCT `prod`.`product_id` FROM `" . DB_PREFIX . "sales` AS `main` "
                . "LEFT JOIN `" . DB_PREFIX . "sales_products` `prod` ON (`main`.`id` = `prod`.`sale_id`) "
                . "WHERE `main`.`category_id` = '" . (int) $category_id . "'");
        $id = [];
        foreach ($sql->rows as $value) {
            $id[] = (int) $value['product_id'];
        }
        return $id;
    }

}
