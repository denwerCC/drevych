<?php

class ModelDesignGallery extends Model {

    public function getGallery($gallery_id) {
        $query = $this->db->query("SELECT DISTINCT * "
                . " FROM `" . DB_PREFIX . "gallery` `main` "
                . " LEFT JOIN `" . DB_PREFIX . "gallery_description` `desc` ON (`main`.`id` = `desc`.`gallery_id`) "
                . " LEFT JOIN `" . DB_PREFIX . "gallery_photo` `photo` ON (`main`.`id` = `photo`.`gallery_id`) "
                . " WHERE `main`.`id` = '" . (int) $gallery_id . "'"
                . " AND `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getGalleries($category_id = 0) {
        $sql = "SELECT * FROM `" . DB_PREFIX . "gallery`AS `main` "
                . "LEFT JOIN `" . DB_PREFIX . "gallery_description` `desc` ON (`main`.`id` = `desc`.`gallery_id`) "
                . "LEFT JOIN `" . DB_PREFIX . "category` `cat` ON (`main`.`category_id` = `cat`.`category_id`) "
                . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "' ";
        $sql .= $category_id > 0 ? " AND `main`.`category_id` = '" . $category_id . "'" : "";
        $query = $this->db->query($sql);
        return $query->rows;
    }

    public function getCategories() {
        $sql = $this->db->query("SELECT DISTINCT `main`.`category_id`, `cat`.`css_class`, `desc`.`name` FROM `" . DB_PREFIX . "gallery` AS `main` "
                . "LEFT JOIN `" . DB_PREFIX . "category` `cat` ON (`main`.`category_id` = `cat`.`category_id`) "
                . "LEFT JOIN `" . DB_PREFIX . "category_description` `desc` ON (`main`.`category_id` = `desc`.`category_id`) "
                . "WHERE `desc`.`language_id` = '" . (int) $this->config->get('config_language_id') . "'");
        return $sql->rows;
    }

    public function getPhotos($gallery_id) {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "gallery_photo` WHERE `gallery_id` = '" . (int) $gallery_id . "' ORDER BY sort_order ASC");
        return $query->rows;
    }

    public function setRating($gallery_id, $value) {
        $user_ip = $this->get_client_ip();
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "rating` WHERE `item_id` = '" . (int) $gallery_id . "' AND `type` = 'gallery' AND `ip` = '" . $user_ip . "'");
        if (empty($query->rows)) {
            $query = $this->db->query("INSERT INTO `" . DB_PREFIX . "rating` (item_id, type, ip, value) "
                    . "VALUES ('" . (int) $gallery_id . "', 'gallery', '" . $user_ip . "', '" . (int) $value . "') ");
            return (int) $value;
        } else {
            $query = $this->db->query("SELECT (SUM(value)/Count(id)) AS rating FROM `" . DB_PREFIX . "rating` WHERE `item_id` = '" . (int) $gallery_id . "' AND `type` = 'gallery'");
            return round($query->row['rating']);
        }
    }

    public function getRating($gallery_id) {
        $query = $this->db->query("SELECT (SUM(value)/Count(id)) AS rating FROM `" . DB_PREFIX . "rating` WHERE `item_id` = '" . (int) $gallery_id . "' AND `type` = 'gallery'");
        return round($query->row['rating']);
    }

    private function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

}
