<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'Служба поддержки';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Обратная связь';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнерская программа';
$_['text_special']      = 'Акции';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Рассылка';
$_['newsblog_href_text']= 'Статьи';
$_['sale_href_text']    = 'Акции';
$_['new_category_href'] = 'Новинки';
$_['text_powered']      = 'Работает на <a href="http://opencart-russia.ru">OpenCart "Русская сборка"</a><br /> %s &copy; %s';
$_['text_enter_form'] = 'Заполните форму';
$_['text_name_and_surname'] = 'Имя и Фамилия';
$_['text_phone_number'] = 'Номер телефона';
$_['text_email'] = 'E-mail';
$_['text_messege'] = 'Cообщение';
$_['text_send'] = 'Отправить';
$_['text_social'] = 'Мы в социальных сетях';
$_['heading_title']     = 'Сравнение товаров';
