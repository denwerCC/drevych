<?php
// Text
$_['text_home']          = 'Главная';
$_['text_menu']          = 'Меню';


$_['text_wishlist']      = 'Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Мой аккаунт';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'Транзакции';
$_['text_download']      = 'Загрузки';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Смотреть Все';
$_['text_items']         = '%s шт.';
$_['phone_text']         = 'Телефон';
$_['gallery_href_text']       = 'Галерея';

$_['text_title_nav_wood']         = 'Древыч <br> Wood';
$_['text_title_nav_smart']         = 'Древыч <br> Laser';
