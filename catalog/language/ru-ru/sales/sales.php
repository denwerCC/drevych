<?php

// Text
$_['text_sale'] = 'Акции';
$_['text_header'] = 'Акции';
$_['text_category_all'] = 'ВСЕ';
$_['text_one_sale'] = 'Акция';
$_['text_left_days_to'] = 'осталось';
$_['text_days'] = 'дней';
$_['text_hours'] = 'часов';
$_['text_minutes'] = 'минут';
$_['text_seconds'] = 'секунд';
$_['text_from'] = 'с';
$_['text_to'] = 'по';
$_['text_button_cart'] = 'Подробнее';
$_['text_empty_sale'] = 'На данный момент акции отсутствуют';
$_['text_to_end_sale'] = 'До конца акции осталось';


$_['months'][0] = "";
$_['months'][1] = "января";
$_['months'][2] = "февраля";
$_['months'][3] = "марта";
$_['months'][4] = "апреля";
$_['months'][5] = "мая";
$_['months'][6] = "июня";
$_['months'][7] = "июля";
$_['months'][8] = "августа";
$_['months'][9] = "сентября";
$_['months'][10] = "октября";
$_['months'][11] = "ноября";
$_['months'][12] = "декабря";
