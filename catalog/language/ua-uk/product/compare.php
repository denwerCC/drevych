<?php
// Heading
$_['heading_title']     = 'Порівняння товарів';

// Text
$_['text_product']      = 'Відомості про товар';
$_['text_name']         = 'Товар';
$_['text_image']        = 'Зображення';
$_['text_price']        = 'Ціна';
$_['text_model']        = 'Модель';
$_['text_manufacturer'] = 'Бренд';
$_['text_availability'] = 'Наявність';
$_['text_instock']      = 'На складі';
$_['text_rating']       = 'Рейтинг';
$_['text_reviews']      = 'На основі %s відгуків.';
$_['text_summary']      = 'Всього';
$_['text_weight']       = 'Вага';
$_['text_dimension']    = 'Розміри (Д х Ш х В)';
$_['text_compare']      = 'Порівняння товару (%s)';
$_['text_success']      = 'Успішно: Виберіть <a href="%s"> %s</a> до вашого <a href="%s">порівняння товарів</a>!';
$_['text_remove']       = 'Успішно: Змінено порівняння товарів!';
$_['text_empty']        = 'Ви не вибрали будь-які товар для порівняння.';