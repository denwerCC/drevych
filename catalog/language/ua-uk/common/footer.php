<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Обслуговування клієнтів';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення';
$_['text_sitemap']      = 'Карта сайту';
$_['text_manufacturer'] = 'Бренди';
$_['text_voucher']      = 'Подарункові ваучери';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Акції';
$_['text_account']      = 'Мої данні';
$_['text_order']        = 'Журнал замовлень';
$_['text_wishlist']     = 'Список побажань';
$_['text_newsletter']   = 'Стрічка новин';
$_['newsblog_href_text']= 'Статті';
$_['sale_href_text']    = 'Акції';
$_['new_category_href'] = 'Новинки';
$_['text_powered']      = 'Працює на <a href="http://www.opencart.com">OpenCart</a><br/> %s &copy; %s';
$_['text_enter_form'] = 'Заповніть форму';
$_['text_name_and_surname'] = 'Ім’я та Прізвище';
$_['text_phone_number'] = 'Номер телефону';
$_['text_email'] = 'E-mail';
$_['text_messege'] = 'Повідомлення';
$_['text_send'] = 'Надіслати';
$_['text_social'] = 'Ми в соціальних мережах';
$_['heading_title']     = 'Порівняння товарів';
