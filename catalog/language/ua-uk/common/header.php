<?php
// Text
$_['text_home']          = 'Головна';
$_['text_menu']          = 'Меню';


$_['text_wishlist']      = 'Список побажань (%s)';
$_['text_shopping_cart'] = 'Кошик';
$_['text_category']      = 'Категорії';
$_['text_account']       = 'Мій аккаунт';
$_['text_register']      = 'Зареєструватись';
$_['text_login']         = 'Увійти';
$_['text_order']         = 'Журнал замовлень';
$_['text_transaction']   = 'Сплаченні рахунки';
$_['text_download']      = 'Завантаження';
$_['text_logout']        = 'Вийти';
$_['text_checkout']      = 'Оформити замовлення';
$_['text_search']        = 'Пошук';
$_['text_all']           = 'Переглянути всі';
$_['text_items']       = '%s шт.';
$_['phone_text']       = 'Телефон';
$_['gallery_href_text']       = 'Галерея';

$_['text_title_nav_wood']         = 'Древич <br> Wood';
$_['text_title_nav_smart']         = 'Древич <br> Laser';

