<?php

// Text
$_['text_sale'] = 'Акції';
$_['text_header'] = 'Акції';
$_['text_category_all'] = 'УСІ';
$_['text_one_sale'] = 'Акція';
$_['text_left_days_to'] = 'залишилося';
$_['text_days'] = 'днів';
$_['text_hours'] = 'годин';
$_['text_minutes'] = 'хвилин';
$_['text_seconds'] = 'секунд';
$_['text_from'] = 'з';
$_['text_to'] = 'до';
$_['text_button_cart'] = 'Детальніше';
$_['text_empty_sale'] = 'На даний момент акції відсутні';
$_['text_to_end_sale'] = 'До кінця акції залишилось';


$_['months'][0] = "";
$_['months'][1] = "січня";
$_['months'][2] = "лютого";
$_['months'][3] = "березня";
$_['months'][4] = "квітня";
$_['months'][5] = "травня";
$_['months'][6] = "червня";
$_['months'][7] = "липня";
$_['months'][8] = "серпня";
$_['months'][9] = "вересня";
$_['months'][10] = "жовтня";
$_['months'][11] = "листопада";
$_['months'][12] = "грудня";
