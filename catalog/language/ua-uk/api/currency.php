<?php
// Text
$_['text_success']     = 'Успіх: Ваша валюта була змінена!';

// Error
$_['error_permission'] = 'У Вас немає доступу до API!';
$_['error_currency']   = 'Увага: код валюти недійсний!';