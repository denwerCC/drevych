<?php

// Text
$_['text_sale'] = 'Sale';
$_['text_header'] = 'Sale';
$_['text_category_all'] = 'All';
$_['text_one_sale'] = 'Sale';
$_['text_left_days_to'] = 'left';
$_['text_days'] = 'days';
$_['text_hours'] = 'hours';
$_['text_minutes'] = 'minutes';
$_['text_seconds'] = 'seconds';
$_['text_from'] = 'from';
$_['text_to'] = 'to';
$_['text_button_cart'] = 'More';
$_['text_empty_sale'] = 'At the moment, there are no sale';
$_['text_to_end_sale'] = 'Time left';


$_['months'][0] = "";
$_['months'][1] = "january";
$_['months'][2] = "february";
$_['months'][3] = "march";
$_['months'][4] = "april";
$_['months'][5] = "may";
$_['months'][6] = "june";
$_['months'][7] = "july";
$_['months'][8] = "august";
$_['months'][9] = "september";
$_['months'][10] = "october";
$_['months'][11] = "november";
$_['months'][12] = "december";
