<?php

// Text
$_['text_gallery'] = 'Gallery';
$_['text_header'] = 'Gallery';
$_['text_category_all'] = 'ALL';
$_['text_one_sale'] = 'Gallery';
$_['text_more_inform'] = 'Review';
$_['text_rating'] = 'Rate';
$_['text_buy'] = 'Order';
$_['look_product_href'] = 'View this product in shop';
