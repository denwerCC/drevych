<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['newsblog_href_text']= 'Articles';
$_['sale_href_text']    = 'Actions';
$_['new_category_href'] = 'Novelty';
$_['text_powered']      = 'Powered By <a href="http://www.opencart.com">OpenCart</a><br /> %s &copy; %s';
$_['text_enter_form'] = 'Fill the form';
$_['text_name_and_surname'] = 'Name and Surename';
$_['text_phone_number'] = 'Phone number';
$_['text_email'] = 'E-mail';
$_['text_messege'] = 'Message';
$_['text_send'] = 'Send';
$_['text_social'] = 'We are on social';
$_['heading_title']     = 'Product Comparison';

