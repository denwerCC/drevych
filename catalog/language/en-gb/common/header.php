<?php
// Text
$_['text_home']          = 'Home';
$_['text_menu']          = 'Menu';


$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Bag';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_items']     = '%s qty';
$_['phone_text']       = 'Phone';
$_['gallery_href_text']       = 'Gallery';

$_['text_title_nav_wood']         = 'Drevych <br> Wood';
$_['text_title_nav_smart']         = 'Drevych <br> Laser';

