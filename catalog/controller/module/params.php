<?php

class ControllerModuleParams extends Controller {

    public function index() {
        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        $category_id = end($parts);

        $this->load->model('catalog/category');

        $category_info = $this->model_catalog_category->getCategory($category_id);

        if ($category_info) {
            
            $this->load->language('module/filter');
            $data['text_select'] = $this->language->get('text_select');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            if (isset($this->request->get['params'])) {
                $get_params = $this->request->get['params'];
            } else {
                $get_params = array();
            }

            $data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));
            
            $this->load->model('catalog/product');

            $data['params_groups'] = array();

            $params_groups = $this->model_catalog_category->getParams();

            if ($params_groups) {
                foreach ($params_groups as $params_group) {
                    $children_data = array();

                    foreach ($params_group['params'] as $parameter) {
                        $params_data = array(
                            'params_category_id' => $category_id,
                            'params_params' => $parameter['params_id']
                        );

                        if (!empty($parameter['back_image'])) {
                            $this->load->model('tool/image');
                            $parameter['back_image'] = $this->model_tool_image->resize($parameter['back_image'], 200, 200);
                        } else {
                            $parameter['back_image'] = '';
                        }
                        if ((isset($get_params[$params_group['secret_key']])  && $get_params[$params_group['secret_key']] == $parameter['value'])) {
                            $checked = 'checked';
                        } else if (!isset($get_params[$params_group['secret_key']])  && $parameter['default']) {
                            $checked = 'checked';
                        } else {
                            $checked = '';
                        }

                        $children_data[] = array(
                            'back_color' => $parameter['back_color'],
                            'back_image' => $parameter['back_image'],
                            'params_id' => $parameter['params_id'],
                            'value' => $parameter['value'],
                            'checked' => $checked,
                            'name' => $parameter['name']
                        );
                    }

                    $data['params_groups'][] = array(
                        'params_group_id' => $params_group['params_group_id'],
                        'custom_class' => $params_group['custom_class'],
                        'name' => $params_group['name'],
                        'secret_key' => $params_group['secret_key'],
                        'params' => $children_data
                    );
                }

                return $this->load->view('module/params', $data);
            }
        }
    }

}
