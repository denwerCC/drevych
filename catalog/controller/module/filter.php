<?php

class ControllerModuleFilter extends Controller {

    public function index() {
        if (isset($this->request->get['path'])) {
            $parts = explode('_', (string) $this->request->get['path']);
        } else {
            $parts = array();
        }

        $category_id = end($parts);

        $this->load->model('catalog/category');

        $category_info = $this->model_catalog_category->getCategory($category_id);

        if ($category_info) {
            $this->load->language('module/filter');

            $data['heading_title'] = $this->language->get('heading_title');
            $data['advanced_search'] = $this->language->get('advanced_search');
            $data['button_filter'] = $this->language->get('button_filter');
            $data['button_filter_clear'] = $this->language->get('button_filter_clear');
            $data['text_select'] = $this->language->get('text_select');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['action'] = str_replace('&amp;', '&', $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url));

            if (isset($this->request->get['filter'])) {
                $data['filter_category'] = explode(',', $this->request->get['filter']);
                $get_filters = explode(',', $this->request->get['filter']);
            } else {
                $get_filters = array();
                $data['filter_category'] = array();
            }

            $this->load->model('catalog/product');

            $data['filter_groups'] = array();

            $filter_groups = $this->model_catalog_category->getCategoryFilters($category_id);

            if ($filter_groups) {
                foreach ($filter_groups as $filter_group) {
                    $childen_data = array();

                    foreach ($filter_group['filter'] as $filter) {
                        $filter_data = array(
                            'filter_category_id' => $category_id,
                            'filter_filter' => $filter['filter_id']
                        );
                        if (!empty($filter['back_image'])) {
                            $this->load->model('tool/image');
                            $filter['back_image'] = $this->model_tool_image->resize($filter['back_image'], 200, 200);
                        } else {
                            $filter['back_image'] = '';
                        }

                        $childen_data[] = array(
                            'back_color' => $filter['back_color'],
                            'back_image' => $filter['back_image'],
                            'filter_id' => $filter['filter_id'],
                            //'name'      => $filter['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : '')
                            'name' => $filter['name'],
                            'checked' => in_array($filter['filter_id'], $get_filters) ? 'checked' : ''
                        );
                    }

                    $data['filter_groups'][] = array(
                        'filter_group_id' => $filter_group['filter_group_id'],
                        'custom_class' => $filter_group['custom_class'],
                        'name' => $filter_group['name'],
                        'filter' => $childen_data
                    );
                }

                return $this->load->view('module/filter', $data);
            }
        }
    }

}
