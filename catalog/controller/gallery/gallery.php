<?php

require_once (DIR_APPLICATION . 'controller/product/product.php');

class ControllerGalleryGallery extends Controller {

    public function index() {

        $this->load->language('gallery/gallery');

        $this->load->model('design/gallery');

        $data['current_query'] = isset(parse_url($_SERVER['REQUEST_URI'])['query']) ? parse_url($_SERVER['REQUEST_URI'])['query'] : '';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_gallery'),
            'href' => $this->url->link('gallery/gallery')
        );

        $data['title'] = $this->language->get('text_header');
        $data['text_category_all'] = $this->language->get('text_category_all');
        $data['text_gallery'] = $this->language->get('text_gallery');
        $data['text_header'] = $this->language->get('text_header');
        $data['look_product_href'] = $this->language->get('look_product_href');
        $data['text_one_sale'] = $this->language->get('text_one_sale');

        $this->document->setTitle($this->language->get('text_header'));

        if ($this->request->server['HTTPS']) {
            $data['server'] = $this->config->get('config_ssl');
        } else {
            $data['server'] = $this->config->get('config_url');
        }

        $data['categoies_array'][0] = array(
            'text' => $data['text_category_all'],
            'css_class' => '',
            'href' => $this->url->link('gallery/gallery'),
            'active' => isset(parse_url($_SERVER['REQUEST_URI'])['query']) && parse_url($_SERVER['REQUEST_URI'])['query'] == parse_url($this->url->link('gallery'))['query'] ? 'active' : ''
        );
        $categories_list = $this->model_design_gallery->getCategories();
        foreach ($categories_list as $value) {
            $data['categoies_array'][$value['category_id']] = array(
                'text' => $value['name'],
                'css_class' => $value['css_class'],
                'href' => $this->url->link('gallery/gallery', 'category_id=' . $value['category_id']),
                'active' => isset($this->request->get['category_id']) && $this->request->get['category_id'] == $value['category_id'] ? 'active' : ''
            );
        }
        if (isset($this->request->get['category_id'])) {
            //****************************************************************//
            //***************************With category************************//
            //****************************************************************//
            $galleries_list = $this->model_design_gallery->getGalleries($this->request->get['category_id']);

            $data['text_more_inform'] = $this->language->get('text_more_inform');

            $data['breadcrumbs'][] = array(
                'text' => $data['categoies_array'][$this->request->get['category_id']]['text'],
                'href' => $data['categoies_array'][$this->request->get['category_id']]['href']
            );

            $data['gallery_list'] = array();
            foreach ($galleries_list as $result) {

                $this->load->model('tool/image');

                if (isset($result['banner']) && is_file(DIR_IMAGE . $result['banner'])) {
                    $baner_gallery = $this->model_tool_image->resize($result['banner'], 400, 400);
                } else {
                    $baner_gallery = '';
                }

                $data['gallery_list'][] = array(
                    'gallery_href' => $this->url->link('gallery/gallery', 'gallery_id=' . $result['id']),
                    'short_description' => html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'),
                    'banner' => $baner_gallery,
                    'css_class' => $result['css_class']
                );
            }

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('gallery/galleries', $data));
        } elseif (isset($this->request->get['gallery_id'])) {
            //********************************************************************************************************************************************************************//
            //***************************With sale id*************************//
            //********************************************************************************************************************************************************************//
            $data['look_product_href'] = $this->language->get('look_product_href');
            $data['text_rating'] = $this->language->get('text_rating');
            $data['text_buy'] = $this->language->get('text_buy');

            $this->load->model('design/gallery');

            $data['gallery_data'] = array();
            $data['gallery_data'] = $gallery_data = $this->model_design_gallery->getGallery($this->request->get['gallery_id']);
            $this->document->setTitle($gallery_data['title']);
            $this->document->setDescription($gallery_data['meta_description']);
            $this->document->setKeywords($gallery_data['meta_keyword']);

            $data['breadcrumbs'][] = array(
                'text' => $data['categoies_array'][$gallery_data['category_id']]['text'],
                'href' => $data['categoies_array'][$gallery_data['category_id']]['href']
            );
            $data['breadcrumbs'][] = array(
                'text' => $gallery_data['title'],
                'href' => $this->url->link('gallery/gallery', 'gallery_id=' . $gallery_data['gallery_id']),
            );

            $photos = $this->model_design_gallery->getPhotos($this->request->get['gallery_id']);

            $data['gallery_data']['photos'] = array();
            $this->load->model('tool/image');
            foreach ($photos as $photo) {
                $data['gallery_data']['photos'][$photo['sort_order']] = $this->model_tool_image->resize($photo['photo'], $this->config->get($this->config->get('config_theme') . '_image_product_height'), $this->config->get($this->config->get('config_theme') . '_image_product_width'));
            }

            $data['gallery_data']['fb_share'] = 'https://www.facebook.com/sharer/sharer.php?u=' . $this->shareUrl($gallery_data['id'], $gallery_data['category_id']);
            $data['gallery_data']['gplus_share'] = 'https://plus.google.com/share?url=' . $this->shareUrl($gallery_data['id'], $gallery_data['category_id']);
            $data['gallery_data']['vk_share'] = 'http://vk.com/share.php?url' . $this->shareUrl($gallery_data['id'], $gallery_data['category_id']);

            $data['gallery_data']['product_id'] = '';

            $parts = parse_url($gallery_data['button_href']);
            if (isset($parts['query'])) {
                parse_str($parts['query'], $query);
            }
            $data['gallery_data']['product_id'] = isset($query['amp;product_id']) && !empty($query['amp;product_id']) ? $query['amp;product_id'] : '';

            $data['continue'] = $this->url->link('common/home');
            $data['gallery_data']['description'] = html_entity_decode($gallery_data['description'], ENT_QUOTES, 'UTF-8');

            $data['gallery_data']['rating'] = $this->model_design_gallery->getRating($this->request->get['gallery_id']);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('gallery/gallery', $data));
        } else {
            //****************************************************************//
            //***************************List all*****************************//
            //****************************************************************//
            $galleries_list = $this->model_design_gallery->getGalleries();

            $data['text_more_inform'] = $this->language->get('text_more_inform');

            $data['gallery_list'] = array();

            foreach ($galleries_list as $result) {

                $this->load->model('tool/image');

                if (isset($result['banner']) && is_file(DIR_IMAGE . $result['banner'])) {
                    $baner_gallery = $this->model_tool_image->resize($result['banner'], 400, 400);
                } else {
                    $baner_gallery = '';
                }
                $data['gallery_list'][] = array(
                    'gallery_href' => $this->url->link('gallery/gallery', 'gallery_id=' . $result['id']),
                    'short_description' => html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'),
                    'banner' => $baner_gallery,
                    'css_class' => $result['css_class']
                );
            }

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('gallery/galleries', $data));
        }
    }

    public function rating() {
        $this->load->model('design/gallery');
        $post = $this->request->post;
        if (isset($post['gallery_id']) && isset($post['value']) && ($post['value'] > 0 && $post['value'] < 6)) {
            $rating = $this->model_design_gallery->setRating($post['gallery_id'], $post['value']);
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode(array('success' => true, 'value' => $rating)));
        } else {
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode(array('success' => false)));
        }
    }

    public function shareUrl($product_id = 0, $path = '') {
        if ((int) $product_id > 0) {
            $url = $this->url->link('gallery/gallery&path=' . $path . '&gallery_id=' . (int) $product_id);
            $url = str_replace(":", "%3A", $url);
            $url = str_replace("&", "%26", $url);
            return $url;
        } else {
            return $this->url->link('common/home');
        }
    }   

}
