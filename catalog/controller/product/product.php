<?php

class ControllerProductProduct extends Controller {

    private $error = array();

    const PineWoodType = 'pine_wood_type';
    const AlderWoodType = 'alder_wood_type';
    const OakWoodType = 'oak_wood_type';
    const AshWoodType = 'ash_wood_type';

    public function index() {
        $this->load->language('product/product');

        //set default currency
        $this->session->data['currency'] = 'UAH';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $this->load->model('catalog/category');

        if (isset($this->request->get['path'])) {
            $path = '';

            $parts = explode('_', (string) $this->request->get['path']);

            $category_id = (int) array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $path)
                    );
                }
            }

            // Set the last category breadcrumb
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }

                $data['breadcrumbs'][] = array(
                    'text' => $category_info['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url)
                );
            }
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['manufacturer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_brand'),
                'href' => $this->url->link('product/manufacturer')
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

            if ($manufacturer_info) {
                $data['breadcrumbs'][] = array(
                    'text' => $manufacturer_info['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url)
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('product/search', $url)
            );
        }

        if (isset($this->request->get['product_id'])) {
            $product_id = (int) $this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        // get custom layout for product

        $this->load->model('design/layout');
        $layout_id = $this->model_catalog_product->getProductLayoutId($product_id);
        $layout_route = $this->model_design_layout->getLayoutRoute($layout_id);

        // send by one click
        //==================================================================
        if (isset($this->request->post['product_by_one_click'])) {
            $url = isset($url) ? $url : '';
            $message = "";
            $message .= "Продукт - " . '<a href="' . $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']) . '">' . $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']) . '</a>' . '<br>';
            $message .=!empty($this->request->post['wood_type']) ? "Тип деревини - " . $this->request->post['wood_type'] . '<br>' : '';
            $message .= "Ім'я та Прізвище - " . $this->request->post['name'] . '<br>';
            $message .= "Телефон - " . $this->request->post['phone'] . '<br>';
            $message .= "Email - " . $this->request->post['email'] . '<br>';
            $message .= "Повідомлення - " . $this->request->post['message'] . '<br>';
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $from = !empty($this->request->post['email']) ? $this->request->post['email'] : $this->config->get('config_email');
            $mail->setFrom($from);
            $mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode(sprintf("Купити а один клік - ", $this->request->post['name']), ENT_QUOTES, 'UTF-8'));
            $mail->setSubject("Купити а один клік");
            $mail->setHtml($message);
            $mail->send();
            $this->response->redirect($this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id']));
        }
        //==================================================================

        if ($product_info) {

            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            //custom calculation
            $data['back_to_list_url'] = end($data['breadcrumbs']);

            $data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'])
            );
            $this->document->setTitle($product_info['meta_title']);
            $this->document->setDescription($product_info['meta_description']);
            $this->document->setKeywords($product_info['meta_keyword']);
            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id']), 'canonical');
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/magnific/magnific-popup.css');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
            $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
            $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

            $data['heading_title'] = $product_info['name'];

            $data['text_select'] = $this->language->get('text_select');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_stock'] = $this->language->get('text_stock');
            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_write'] = $this->language->get('text_write');
            $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
            $data['text_note'] = $this->language->get('text_note');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_related'] = $this->language->get('text_related');
            $data['spase_size'] = $this->language->get('spase_size');
            $data['text_or'] = $this->language->get('text_or');

            $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['entry_qty'] = $this->language->get('entry_qty');
            $data['entry_name'] = $this->language->get('entry_name');
            $data['entry_review'] = $this->language->get('entry_review');
            $data['entry_rating'] = $this->language->get('entry_rating');
            $data['entry_good'] = $this->language->get('entry_good');
            $data['entry_bad'] = $this->language->get('entry_bad');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_one_click'] = $this->language->get('button_one_click');
            $data['button_recalculate'] = $this->language->get('button_recalculate');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_upload'] = $this->language->get('button_upload');
            $data['button_continue'] = $this->language->get('button_continue');

            //custom calculation
            $data['back_to_list'] = $this->language->get('back_to_list');
            $data['size_title'] = $this->language->get('size_title');

            $data['wood']['pine'] = $this->language->get('wood_type_pine');
            $data['wood']['alder'] = $this->language->get('wood_type_alder');
            $data['wood']['oak'] = $this->language->get('wood_type_oak');
            $data['wood']['ash'] = $this->language->get('wood_type_ash');

            $data['hardness'] = $this->language->get('hardness');
            $data['structure'] = $this->language->get('structure');
            $data['prestige'] = $this->language->get('prestige');

            $this->load->model('catalog/review');

            $data['tab_description'] = $this->language->get('tab_description');
            $data['tab_attribute'] = $this->language->get('tab_attribute');
            $data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

            $data['product_id'] = (int) $this->request->get['product_id'];
            $data['manufacturer'] = $product_info['manufacturer'];
            $data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id']);
            $data['model'] = $product_info['model'];
            $data['reward'] = $product_info['reward'];
            $data['points'] = $product_info['points'];
            $pathUrl = isset($this->request->get['path']) ? $this->request->get['path'] : '';
            $data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
            $data['fb_share'] = 'https://www.facebook.com/sharer/sharer.php?u=' . $this->shareUrl($product_id, $pathUrl);
            $data['gplus_share'] = 'https://plus.google.com/share?url=' . $this->shareUrl($product_id, $pathUrl);
            $data['vk_share'] = 'http://vk.com/share.php?url' . $this->shareUrl($product_id, $pathUrl);

            if ($product_info['quantity'] <= 0) {
                $data['stock'] = $product_info['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $data['stock'] = $product_info['quantity'];
            } else {
                $data['stock'] = $this->language->get('text_instock');
            }

            $this->load->model('tool/image');

            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'));
            } else {
                $data['popup'] = '';
            }

            if (isset($this->session->data["def_image_" . $this->request->get['product_id']])) {
                $data['thumb'] = $this->model_tool_image->resize($this->session->data["def_image_" . $this->request->get['product_id']], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
            } elseif ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
            } else {
                $data['thumb'] = '';
            }

            $data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

            foreach ($results as $result) {
                $data['images'][] = array(
                    'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
                    'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
                );
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $data['price'] = false;
            }

            if ((float) $product_info['special']) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $data['special'] = false;
            }

            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float) $product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
            } else {
                $data['tax'] = false;
            }

            $discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

            $data['discounts'] = array();

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
                );
            }

            if ($product_info['calculate']) {

                $this->cache->delete('product');

                if ($this->request->server['HTTPS']) {
                    $data['server'] = $this->config->get('config_ssl');
                } else {
                    $data['server'] = $this->config->get('config_url');
                }

                $data['options'] = array();

                $data['wood_types'] = $this->model_catalog_product->getProductWoodTypes($this->request->get['product_id']);

                $params_default = $this->model_catalog_category->getDefaultParams();
                $customTypesPrices = $customParams = [];
                if (isset($this->request->get['params'])) {
                    $customParams = $this->request->get['params'];
                } else {
                    $customParams = $params_default;
                }
                $customTypesPrices = ControllerProductProduct::calculate_conf_price(['returnPriceTypes' => true, 'customPost' => ['product_id' => $product_info['product_id']], 'customParams' => $customParams]);
                foreach ($data['wood_types'] as $i => $value) {
                    $data['wood_types'][$i]['def_price'] = $customTypesPrices['success'][$value['type']];
                    $data['wood_types'][$i]['def_price_value'] = $customTypesPrices['success'][$value['type']];
                }
                $product_options = $this->model_catalog_product->getProductOptions($this->request->get['product_id']);

                //get window types
                $data["window_types"] = [];
                foreach ($product_options as $option) {
                    $product_option_value_data = array();

                    //get window types                    
                    if ($option["secret_key"] == 'window_type_key') {
                        $data["window_types"] = $option['product_option_value'];
                    }

                    if (isset($customParams['var_b-a_key']) && !empty($customParams['var_b-a_key'])) {
                        $ab = explode('-', $customParams['var_b-a_key']);
                        $var_a = str_replace('.', ',', $ab[1] / 100);
                        $var_b = str_replace('.', ',', $ab[0] / 100);
                    } else {
                        $var_a = $var_b = 0;
                    }
                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ($option["secret_key"] == "type_plus_price") {
                                $price = $this->currency->format((float) $option_value['price'], $this->session->data['currency']);
                            } else {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
                                    $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $price = false;
                                }
                            }
                            $priceValue = str_replace(',', '.', $price);
                            if (isset($customParams[$option['secret_key']]) && str_replace(',', '.', $customParams[$option['secret_key']]) == floatval($priceValue)) {
                                $selected = true;
                            } else {
                                $selected = false;
                            }

                            if (isset($this->session->data["def_image_" . $this->request->get['product_id']]) && $this->session->data["def_image_" . $this->request->get['product_id']] == $option_value['image']) {
                                $selected = true;
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                //'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'image' => $this->model_tool_image->resize($option_value['image'], 1280, 960),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix'],
                                'selected' => $selected,
                            );
                        }
                    }
                    $data['options'][] = array(
                        'product_option_id' => $option['product_option_id'],
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'secret_key' => $option['secret_key'],
                        $option['secret_key'] => true,
                        'visible' => $option['visible'],
                        'product_option_value' => $product_option_value_data,
                        'var_a' => $var_a,
                        'var_b' => $var_b
                    );
                }
            } else {
                $data['options'] = array();

                foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                    $product_option_value_data = array();

                    foreach ($option['product_option_value'] as $option_value) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float) $option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $price = false;
                            }

                            $product_option_value_data[] = array(
                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }

                    $data['options'][] = array(
                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required'],
                        'secret_key' => $option['secret_key'],
                        'visible' => $option['visible']
                    );
                }
            }

            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }

            $data['review_status'] = $this->config->get('config_review_status');

            if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
                $data['review_guest'] = true;
            } else {
                $data['review_guest'] = false;
            }

            if ($this->customer->isLogged()) {
                $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
            } else {
                $data['customer_name'] = '';
            }

            $data['reviews'] = sprintf($this->language->get('text_reviews'), (int) $product_info['reviews']);
            $data['rating'] = (int) $product_info['rating'];

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array) $this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }

            $data['share'] = $this->url->link('product/product', 'product_id=' . (int) $this->request->get['product_id']);

            $data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

            $data['products'] = array();

            $results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);

            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float) $result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int) $result['rating'];
                } else {
                    $rating = false;
                }
                $pathUrl = isset($this->request->get['path']) ? $this->request->get['path'] : '';
                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id']),
                    'fb_share' => 'https://www.facebook.com/sharer/sharer.php?u=' . $this->shareUrl($result['product_id'], $pathUrl),
                    'gplus_share' => 'https://plus.google.com/share?url=' . $this->shareUrl($result['product_id'], $pathUrl),
                    'vk_share' => 'http://vk.com/share.php?url' . $this->shareUrl($result['product_id'], $pathUrl)
                );
            }

            $data['tags'] = array();

            if ($product_info['tag']) {
                $tags = explode(',', $product_info['tag']);

                foreach ($tags as $tag) {
                    $data['tags'][] = array(
                        'tag' => trim($tag),
                        'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                    );
                }
            }

            $data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

            $this->model_catalog_product->updateViewed($this->request->get['product_id']);

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $layout_route = !empty($layout_route) ? $layout_route : 'product/product';


            $this->response->setOutput($this->load->view($layout_route, $data));
        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    public function review() {
        $this->load->language('product/product');

        $this->load->model('catalog/review');

        $data['text_no_reviews'] = $this->language->get('text_no_reviews');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['reviews'] = array();

        $review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

        $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

        foreach ($results as $result) {
            $data['reviews'][] = array(
                'author' => $result['author'],
                'text' => nl2br($result['text']),
                'rating' => (int) $result['rating'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $pagination = new Pagination();
        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = 5;
        $pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

        $this->response->setOutput($this->load->view('product/review', $data));
    }

    public function write() {
        $this->load->language('product/product');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                $json['error'] = $this->language->get('error_name');
            }

            if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
                $json['error'] = $this->language->get('error_text');
            }

            if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
                $json['error'] = $this->language->get('error_rating');
            }

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array) $this->config->get('config_captcha_page'))) {
                $captcha = $this->load->controller('captcha/' . $this->config->get('config_captcha') . '/validate');

                if ($captcha) {
                    $json['error'] = $captcha;
                }
            }

            if (!isset($json['error'])) {
                $this->load->model('catalog/review');

                $this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

                $json['success'] = $this->language->get('text_success');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getRecurringDescription() {
        $this->load->language('product/product');
        $this->load->model('catalog/product');

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        if (isset($this->request->post['recurring_id'])) {
            $recurring_id = $this->request->post['recurring_id'];
        } else {
            $recurring_id = 0;
        }

        if (isset($this->request->post['quantity'])) {
            $quantity = $this->request->post['quantity'];
        } else {
            $quantity = 1;
        }

        $product_info = $this->model_catalog_product->getProduct($product_id);
        $recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

        $json = array();

        if ($product_info && $recurring_info) {
            if (!$json) {
                $frequencies = array(
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                );

                if ($recurring_info['trial_status'] == 1) {
                    $price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
                } else {
                    $trial_text = '';
                }

                $price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                if ($recurring_info['duration']) {
                    $text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                } else {
                    $text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                }

                $json['success'] = $text;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function setDefImage() {
        if (isset($this->request->post['product_id']) && isset($this->request->post['product_image_url'])) {
            $product_id = (int) $this->request->post['product_id'];
            $image_url = $this->request->post['product_image_url'];
            $this->session->data["def_image_$product_id"] = $image_url;
            $json['success'] = true;
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function shareUrl($product_id = 0, $path = '') {
        if ((int) $product_id > 0) {
            $url = $this->url->link('product/product&path=' . $path . '&product_id=' . (int) $product_id);
            $url = str_replace(":", "%3A", $url);
            $url = str_replace("&", "%26", $url);
            return $url;
        } else {
            return $this->url->link('common/home');
        }
    }

    /**
     * 
     * @param type $params
     * @return type
     */
    public static function calculate_conf_price($params = []) {
        global $registry;
        $controller = new ControllerProductProduct($registry);
        //url http://drevych.ua/index.php?route=product/product/calculate_conf_price
        $controller->load->language('product/product');
        $controller->load->model('catalog/product');
        $post = isset($params['customPost']) && !empty($params['customPost']) ? $params['customPost'] : $controller->request->post;
        //var_dump($controller->request->post);die;
        if (isset($post['product_id'])) {
            $product_id = $post['product_id'];
            $option = isset($post['option']) ? $post['option'] : [];
            $json = [];
            $product_options = $controller->model_catalog_product->getProductOptions($product_id);
            $product_info = $controller->model_catalog_product->getProduct($product_id);
            //var_dump($product_options);
            foreach ($product_options as $product_option) {
                if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
                    $json['error']['option'][$product_option['product_option_id']] = sprintf($controller->language->get('error_required'), $product_option['name']);
                }
            }
            // get constants and wood types
            $productWoodTypes = $controller->model_catalog_product->getProductWoodTypes($product_id);
            if (empty($productWoodTypes)) {
                isset($json['error']['message']) ? $json['error']['message'] .= "\nВідсутні типи деревини" : $json['error']['message'] = "Відустні типи деревини";
            } else {
                //$kyrs = $controller->currency->format(1, $controller->session->data['currency'], '', false);
                $kyrs = $controller->config->get('config_calc_kurs');
                //constant variables
                $def_wl = $controller->config->get('config_def_wl');
                $def_p = $controller->config->get('config_def_p');
                $def_kar = $controller->config->get('config_def_kar');
                $def_k = $controller->config->get('config_def_k');
                $def_l = $controller->config->get('config_def_l');
                $def_por = $controller->config->get('config_def_por');
                $def_lak = $controller->config->get('config_def_lak');
                $def_s = $product_info['def_s'];
                $def_m_sos = $controller->config->get('config_def_m_sos');
                $def_m_vil = $controller->config->get('config_def_m_vil');
                $def_m_jasen = $controller->config->get('config_def_m_jasen');
                $def_m_dub = $controller->config->get('config_def_m_dub');

                //get array $optionKeyId['key'] = id
                $optionKeyId = $controller->model_catalog_product->getProductOptionKeyId($product_id);
                //get radio button id - value
                $optionsValueId = $controller->model_catalog_product->getOptIdValue($product_id);
                //changed variables
                if (isset($params['customParams'])) {
                    if (isset($params['customParams']['var_b-a_key']) && !empty($params['customParams']['var_b-a_key'])) {
                        $ab = explode('-', $params['customParams']['var_b-a_key']);
                        $var_a_key = $ab[1] / 100;
                        $var_b_key = $ab[0] / 100;
                    } else {
                        $var_a_key = $var_b_key = '';
                    }
                    $var_t_key = isset($params['customParams']['var_t_key']) && !empty($params['customParams']['var_t_key']) ? str_replace(',', '.', $params['customParams']['var_t_key']) : '';
                    $var_kt_key = isset($params['customParams']['var_kt_key']) && !empty($params['customParams']['var_kt_key']) ? str_replace(',', '.', $params['customParams']['var_kt_key']) : '';
                    $var_ltype_key = isset($params['customParams']['var_ltype_key']) && !empty($params['customParams']['var_ltype_key']) ? str_replace(',', '.', $params['customParams']['var_ltype_key']) : '';
                    $var_kartype_key = isset($params['customParams']['var_kartype_key']) && !empty($params['customParams']['var_kartype_key']) ? str_replace(',', '.', $params['customParams']['var_kartype_key']) : '';
                    $var_threshold_key = isset($params['customParams']['threshold_key']) && !empty($params['customParams']['threshold_key']) ? str_replace(',', '.', $params['customParams']['threshold_key']) : '';
                    $var_threshold_n_key = isset($params['customParams']['var_threshold_n_key']) && !empty($params['customParams']['var_threshold_n_key']) ? str_replace(',', '.', $params['customParams']['var_threshold_n_key']) : '';
                    $var_l_n_key = isset($params['customParams']['var_l_n_key']) && !empty($params['customParams']['var_l_n_key']) ? str_replace(',', '.', $params['customParams']['var_l_n_key']) : '';
                    $var_lak_e_key = isset($params['customParams']['var_lak_e_key']) &&
                            !empty($params['customParams']['var_lak_e_key']) ? str_replace(',', '.', $params['customParams']['var_lak_e_key']) : '';
                    $var_lak_pat_key = isset($params['customParams']['var_lak_pat_key']) &&
                            !empty($params['customParams']['var_lak_pat_key']) ? str_replace(',', '.', $params['customParams']['var_lak_pat_key']) : '';
                } else {
                    $var_a_key = isset($optionKeyId['var_a_key']) && isset($option[$optionKeyId['var_a_key']]) && isset($option[$optionKeyId['var_a_key']]) ? str_replace(',', '.', $option[$optionKeyId['var_a_key']]) : '';
                    $var_b_key = isset($optionKeyId['var_b_key']) && isset($option[$optionKeyId['var_b_key']]) && isset($option[$optionKeyId['var_b_key']]) ? str_replace(',', '.', $option[$optionKeyId['var_b_key']]) : '';
                    $var_t_key = isset($optionKeyId['var_t_key']) && isset($option[$optionKeyId['var_t_key']]) && isset($option[$optionKeyId['var_t_key']]) ? str_replace(',', '.', $option[$optionKeyId['var_t_key']]) : '';
                    $var_kt_key = isset($optionKeyId['var_kt_key']) && isset($option[$optionKeyId['var_kt_key']]) && isset($optionsValueId[$option[$optionKeyId['var_kt_key']]]) ? $optionsValueId[$option[$optionKeyId['var_kt_key']]] : '';
                    $var_ltype_key = isset($option[$optionKeyId['var_ltype_key']]) && isset($optionsValueId[$option[$optionKeyId['var_ltype_key']]]) ? $optionsValueId[$option[$optionKeyId['var_ltype_key']]] : '';
                    $var_kartype_key = isset($optionKeyId['var_kartype_key']) && isset($option[$optionKeyId['var_kartype_key']]) && isset($optionsValueId[$option[$optionKeyId['var_kartype_key']]]) ? $optionsValueId[$option[$optionKeyId['var_kartype_key']]] : '';
                    $var_threshold_key = isset($optionKeyId['threshold_key']) && isset($option[$optionKeyId['threshold_key']]) && isset($optionsValueId[$option[$optionKeyId['threshold_key']]]) ? (float) $optionsValueId[$option[$optionKeyId['threshold_key']]] : 0;
                    $var_threshold_n_key = isset($optionKeyId['var_threshold_n_key']) && isset($option[$optionKeyId['var_threshold_n_key']]) && isset($optionsValueId[$option[$optionKeyId['var_threshold_n_key']]]) ? (float) $optionsValueId[$option[$optionKeyId['var_threshold_n_key']]] : '';
                    $var_l_n_key = isset($optionKeyId['var_l_n_key']) && isset($option[$optionKeyId['var_l_n_key']]) && isset($optionsValueId[$option[$optionKeyId['var_l_n_key']]]) ? (float) $optionsValueId[$option[$optionKeyId['var_l_n_key']]] : '';
                    $var_lak_e_key = isset($option[$optionKeyId['var_lak_e_key']]) &&
                            isset($optionsValueId[$option[$optionKeyId['var_lak_e_key']]]) ? $optionsValueId[$option[$optionKeyId['var_lak_e_key']]] : 1;
                    $var_lak_pat_key = isset($option[$optionKeyId['var_lak_pat_key']]) &&
                            isset($optionsValueId[$option[$optionKeyId['var_lak_pat_key']]]) ? $optionsValueId[$option[$optionKeyId['var_lak_pat_key']]] : 1;
                }
                //if product white color then lak - yes
                $var_lak_e_key = !empty($product_info['white_color_lak']) ? $product_info['white_color_lak'] : $var_lak_e_key;
                /* var_dump($var_a_key);
                  var_dump($var_b_key);
                  var_dump($var_kt_key);
                  var_dump($var_ltype_key);
                  var_dump($var_kartype_key);
                  var_dump($var_threshold_key);
                  var_dump($var_threshold_n_key); */
                foreach ($productWoodTypes as $type) {
                    $price = $type['def_price'];
                    if ($type['type'] == self::PineWoodType) {
                        $def_m = $def_m_sos;
                    } elseif ($type['type'] == self::AlderWoodType) {
                        $def_m = $def_m_vil;
                    } elseif ($type['type'] == self::AshWoodType) {
                        $def_m = $def_m_jasen;
                    } elseif ($type['type'] == self::OakWoodType) {
                        $def_m = $def_m_dub;
                    }
                    // calculation canvas if isset height and weight
                    //var_dump($kyrs);
                    if (!empty($var_a_key) && !empty($var_b_key) &&
                            !empty($def_p) &&
                            !empty($kyrs) &&
                            !empty($def_s) &&
                            !empty($def_m)) {
                        $canvas = $controller->calculateCanvas($var_a_key
                                , $var_b_key
                                , $def_p
                                , $kyrs
                                , $def_s
                                , $def_m);
                        //var_dump($canvas);
                        $price = $canvas;

                        // check and calculate canvas lak
                        if (!empty($var_lak_e_key) && !empty($var_lak_pat_key) && !empty($def_lak)) {
                            $canvasLak = $controller->calculateCanvasLak($var_a_key
                                    , $var_b_key
                                    , $kyrs
                                    , $var_lak_e_key
                                    , $var_lak_pat_key
                                    , $def_lak);
                            //var_dump($canvasLak);
                            $price += $canvasLak;
                        }
                        // check and calculate box
                        if (!empty($var_kt_key) && !empty($var_t_key)) {
                            $box = $controller->calculateBox($var_a_key
                                    , $var_b_key
                                    , $var_t_key
                                    , $def_k
                                    , $def_m
                                    , $var_kt_key
                                    , $kyrs
                                    , $var_lak_e_key
                                    , $var_lak_pat_key);
                            //var_dump($box);
                            $price += $box;
                        }

                        // check and calculate welt
                        if (!empty($var_ltype_key) && !empty($var_l_n_key)) {
                            $welt = $controller->calculateWelt($var_a_key
                                    , $var_b_key
                                    , $def_wl
                                    , $def_l
                                    , $def_lak
                                    , $var_ltype_key
                                    , $kyrs
                                    , $var_lak_e_key
                                    , $var_lak_pat_key
                                    , $def_m,
                                      $var_l_n_key);
                            //var_dump($welt);
                            $price += $welt;
                        }

                        // check and calculate cornice
                        if (!empty($var_kartype_key)) {
                            $cornice = $controller->calculateCornice($var_b_key
                                    , $def_kar
                                    , $var_kartype_key
                                    , $kyrs
                                    , $def_lak
                                    , $var_lak_e_key
                                    , $var_lak_pat_key
                                    , $def_m
                                    , $var_threshold_n_key);
                            $price += $cornice;
                            //var_dump($cornice);
                        }
                        if ((bool) ($var_threshold_key)) {
                            $doorstep = $controller->calculateDoorstep($var_b_key
                                    , $var_t_key
                                    , $kyrs
                                    , $def_por
                                    , $def_lak
                                    , $var_lak_e_key
                                    , $var_lak_pat_key);
                            //var_dump($doorstep);
                            $price += $doorstep;
                        }
                        //$json['success'][$type['type']] = $controller->currency->format($price, $controller->session->data['currency'], 1);
                        $json['success'][$type['type']] = $controller->currency->format($price, $controller->session->data['currency']);
                    }
                }
            }
            if (isset($params['returnPrice']) && $params['returnPrice']) {
                return $json['success'][$post['option']['wood']];
            } else if (isset($params['returnPriceTypes']) && $params['returnPriceTypes']) {
                return $json;
            } else {
                $controller->response->addHeader('Content-Type: application/json');
                $controller->response->setOutput(json_encode($json));
            }
        } else {
            if (isset($params['returnPrice']) && $params['returnPrice']) {
                return [];
            } else {
                $json['error']['message'] = "Відсутній ід продукту";
                $controller->response->addHeader('Content-Type: application/json');
                $controller->response->setOutput(json_encode($json));
            }
        }
    }

    /*
     * A*B*2=S
     * $полотна=S*P*KYRS*S(1-5)*M sos, Vil…
     * =E24*D65*D24*D85*D80
     */

    private function calculateCanvas($var_a_key
    , $var_b_key
    , $def_p
    , $kyrs
    , $def_s
    , $def_m) {
        return ($var_a_key * $var_b_key * 2) * $def_p * $kyrs * $def_s * $def_m;
    }

    /*
     * $LAK=S*E*PAT*KURS*Lak
     * =E24*D24*D75*H76*H78
     */

    private function calculateCanvasLak($var_a_key
    , $var_b_key
    , $kyrs
    , $var_lak_e_key
    , $var_lak_pat_key
    , $def_lak) {
        return ($var_a_key * $var_b_key * 2) * $kyrs * $def_lak * $var_lak_e_key * $var_lak_pat_key  * 0.5;
    }

    /*
     * $K=(2A+1B)*T*K*KT*$*LAK*kurs*E*PAT
     * =(2*B24+C24)*2*F24*D70*D80*G70*D24*H76*H78
     */

    private function calculateBox($var_a_key
    , $var_b_key
    , $var_t_key
    , $def_k
    , $def_m
    , $var_kt
    , $kyrs
    , $var_lak_e_key
    , $var_lak_pat_key) { 
        return (2 * $var_a_key + $var_b_key) * 2 * $var_t_key * $def_k * $def_m * $var_kt * $kyrs * $var_lak_e_key * $var_lak_pat_key;
    }

    /*
     * $L= ((2*A+B)*WL)*(к-сть стор. 1,2,0)*L*LType*KYRS*E*PAT*M sos, Vil…
     * =((2*B24+C24)*1*G24)*1*D71*D75*G71*D24*H76*H78*0,5*D80
     */

    private function calculateWelt($var_a_key
    , $var_b_key
    , $def_wl
    , $def_l
    , $def_lak
    , $var_ltype_key
    , $kyrs
    , $var_lak_e_key
    , $var_lak_pat_key
    , $def_m
    , $var_l_n_key) {
        return ((2 * $var_a_key + $var_b_key) * 1 * $def_wl) * $var_l_n_key * $def_l * $def_lak * $var_ltype_key * $kyrs * $var_lak_e_key * $var_lak_pat_key * 0.5 * $def_m;
    }

    /*
     * $Kar=(B*Kar*KarType*KYRS*Lak*E*PAT*M sos, Vil…)
     * =C24*D66*G67*D24*D75*H76*H78*D80
     */
    /*
     * New formula from 23.05.17
     *  = ( ( C24*G66*D66*D24*D80 ) + ( C24*G66*D75*0,5*H76*H78*D24 ) * 1) 
     */

    private function calculateCornice($var_b_key
    , $def_kar
    , $var_kartype_key
    , $kyrs
    , $def_lak
    , $var_lak_e_key
    , $var_lak_pat_key
    , $def_m
    , $var_threshold_n_key) {
        //$var_b_key * $def_kar * $var_kartype_key * $kyrs * $def_lak * $var_lak_e_key * $var_lak_pat_key * $def_m)
        //var_dump(($var_b_key * $var_kartype_key * $def_kar * $kyrs * $def_m) + ($var_b_key * $var_kartype_key * $def_lak * 0.5 * $var_lak_e_key * $var_lak_pat_key * $kyrs) * $var_threshold_n_key);
        return (
                ($var_b_key * $var_kartype_key * $def_kar * $kyrs * $def_m) +
                ($var_b_key * $var_kartype_key * $def_lak * 0.5 * $var_lak_e_key * $var_lak_pat_key * $kyrs)
                ) * $var_threshold_n_key;
    }

    /*
     * $Por=(B*T)*KYRS*Por*Lak*E*PAT)
     * =C24*F24*D24*D75*H76*H78
     */

    /*
     * New formula from 23.05.17
     * = (C24*D24*F24*D72) + (D75*H76*H78*D24*C24*F24*0,5)
     */

    private function calculateDoorstep($var_b_key
    , $var_t_key
    , $kyrs
    , $def_por
    , $def_lak
    , $var_lak_e_key
    , $var_lak_pat_key) {
        //return $var_b_key * $var_t_key * $kyrs * $def_lak * $var_lak_e_key * $var_lak_pat_key;
        return ($var_b_key * $kyrs * $var_t_key * $def_por) + ($def_lak * $var_lak_e_key * $var_lak_pat_key * $kyrs * $var_b_key * $var_t_key * 0.5);
    }

}
