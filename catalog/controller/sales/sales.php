<?php

require_once (DIR_APPLICATION . 'controller/product/product.php');

class ControllerSalesSales extends Controller {

    public function index() {

        $this->load->language('sales/sales');

        $this->load->model('catalog/sales');

        $data['current_query'] = isset(parse_url($_SERVER['REQUEST_URI'])['query']) ? parse_url($_SERVER['REQUEST_URI'])['query'] : '';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_sale'),
            'href' => $this->url->link('sales/sales')
        );

        $data['title'] = $this->language->get('text_header');
        $data['text_category_all'] = $this->language->get('text_category_all');
        $data['text_sale'] = $this->language->get('text_sale');
        $data['text_header'] = $this->language->get('text_header');
        $data['text_category_all'] = $this->language->get('text_category_all');
        $data['text_days'] = $this->language->get('text_days');
        $data['text_hours'] = $this->language->get('text_hours');
        $data['text_minutes'] = $this->language->get('text_minutes');
        $data['text_seconds'] = $this->language->get('text_seconds');
        $data['text_one_sale'] = $this->language->get('text_one_sale');
        $data['button_cart'] = $this->language->get('text_button_cart');
        $data['text_empty_sale'] = $this->language->get('text_empty_sale');
        $data['text_to_end_sale'] = $this->language->get('text_to_end_sale');

        $this->document->setTitle($this->language->get('text_header'));

        if ($this->request->server['HTTPS']) {
            $data['server'] = $this->config->get('config_ssl');
        } else {
            $data['server'] = $this->config->get('config_url');
        }

        $data['categoies_array'][0] = array(
            'text' => $data['text_category_all'],
            'href' => $this->url->link('sales/sales'),
            'active' => isset(parse_url($_SERVER['REQUEST_URI'])['query']) && parse_url($_SERVER['REQUEST_URI'])['query'] == parse_url($this->url->link('sales'))['query'] ? 'active' : ''
        );
        $categories_list = $this->model_catalog_sales->getCategories();
        foreach ($categories_list as $value) {
            $data['categoies_array'][$value['category_id']] = array(
                'text' => $value['name'],
                'href' => $this->url->link('sales/sales', 'category_id=' . $value['category_id']),
                'active' => isset($this->request->get['category_id']) && $this->request->get['category_id'] == $value['category_id'] ? 'active' : ''
            );
        }
        if (isset($this->request->get['category_id'])) {
            //*******************************************************************************************************************************************************************//
            //***************************With category************************//
            //*******************************************************************************************************************************************************************//
            $sales_list = $this->model_catalog_sales->getSales($this->request->get['category_id']);
            $this->language->get('text_home');
            $data['breadcrumbs'][] = array(
                'text' => $data['categoies_array'][$this->request->get['category_id']]['text'],
                'href' => $data['categoies_array'][$this->request->get['category_id']]['href']
            );
            foreach ($sales_list as $result) {
                // days to
                $date_to = strtotime($result['date_to']);
                $timeleft = $date_to - time();
                $daysleft = round((($timeleft / 24) / 60) / 60);
                $daysleft = $daysleft < 10 ? '0' . $daysleft : $daysleft;
                // create image
                $this->load->model('tool/image');
                if (isset($result['baner_category']) && is_file(DIR_IMAGE . $result['baner_category'])) {
                    $baner_category = $this->model_tool_image->resize($result['baner_category'], 400, 400);
                } else {
                    $baner_category = '';
                }
                // date from to
                $months = $this->language->get('months');
                $month_from = $months[date("n", strtotime($result['date_from']))];
                $day_from = date("j", strtotime($result['date_from']));
                $month_to = $months[date("n", strtotime($result['date_to']))];
                $day_to = date("j", strtotime($result['date_to']));
                $data['sales_list'][] = array(
                    'sale_href' => $this->url->link('sales/sales', 'sale_id=' . $result['sale_id']),
                    'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                    'baner_category' => $baner_category,
                    'days_left' => $this->language->get('text_left_days_to') . ' ' . $daysleft . ' ' . $this->language->get('text_days'),
                    'from_to_dates' => $this->language->get('text_from') . ' ' . $day_from . ' ' . $month_from . ' ' . $this->language->get('text_to') . ' ' . $day_to . ' ' . $month_to);
            }

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('sales/sales', $data));
        } elseif (isset($this->request->get['sale_id'])) {
            //********************************************************************************************************************************************************************//
            //***************************With sale id*************************//
            //********************************************************************************************************************************************************************//
            $this->load->model('catalog/sales');
            $results = $this->model_catalog_sales->getSale($this->request->get['sale_id']);
            $sale_data = $results['sale'];
            $this->document->setTitle($sale_data['title']);
            $this->document->setDescription($sale_data['meta_description']);
            $this->document->setKeywords($sale_data['meta_keyword']);

            $this->load->model('tool/image');
            if (isset($sale_data['baner']) && is_file(DIR_IMAGE . $sale_data['baner'])) {
                $data['baner'] = $this->model_tool_image->resize($sale_data['baner'], 600, 600);
            } else {
                $data['baner'] = '';
            }
            $data['category_href'] = $this->url->link('sales/sales', 'category_id=' . $sale_data['category_id']);
            $data['category_text'] = $data['categoies_array'][$sale_data['category_id']]['text'];
            $data['date_to'] = $sale_data['date_to'];
            $data['breadcrumbs'][] = array(
                'text' => $data['categoies_array'][$sale_data['category_id']]['text'],
                'href' => $data['categoies_array'][$sale_data['category_id']]['href']
            );
            $data['breadcrumbs'][] = array(
                'text' => $sale_data['title'],
                'href' => $this->url->link('sales/sales', 'sale_id=' . $sale_data['sale_id']),
            );

            $this->load->language('product/category');
            $data['p_name'] = $this->language->get('p_name');
            $data['p_color'] = $this->language->get('p_color');
            $data['p_serie'] = $this->language->get('p_serie');
            $data['p_wood_t'] = $this->language->get('p_wood_t');
            $data['p_window_t'] = $this->language->get('p_window_t');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_wishlist'] = $this->language->get('button_wishlist');

            $this->load->language('product/product');
            $data['wood']['pine'] = $this->language->get('wood_type_pine');
            $data['wood']['alder'] = $this->language->get('wood_type_alder');
            $data['wood']['oak'] = $this->language->get('wood_type_oak');
            $data['wood']['ash'] = $this->language->get('wood_type_ash');

            $this->load->model('catalog/category');
            $params_default = $this->model_catalog_category->getDefaultParams();
            foreach ($results['products'] as $result) {
                $this->load->model('tool/image');
                if (isset($this->session->data["def_image_" . $result['product_id']])) {
                    $image = $this->model_tool_image->resize($this->session->data["def_image_" . $result['product_id']], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'));
                } elseif ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                }
                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float) $result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float) $result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int) $result['rating'];
                } else {
                    $rating = false;
                }

                $attribute_colors = [];
                if ($result['calculate']) {
                    $attribute_colors = $this->model_catalog_product->getProductColorAttribute($result['product_id'], 7);
                    $woodType = $this->model_catalog_product->getProductWoodTypes($result['product_id']);
                    $customTypesPrices = $customParams = [];
                    if (isset($this->request->get['params'])) {
                        $customParams = $this->request->get['params'];
                    } else {
                        $customParams = $params_default;
                    }
                    $customTypesPrices = ControllerProductProduct::calculate_conf_price(['returnPriceTypes' => true, 'customPost' => ['product_id' => $result['product_id']], 'customParams' => $customParams]);
                    foreach ($woodType as $i => $value) {
                        $woodType[$i]['def_price'] = isset($customTypesPrices['success']) ? $customTypesPrices['success'][$value['type']] : '';
                    }

                    //get window types
                    $window_types = $this->model_catalog_product->getProductWindowTypes($result['product_id']);
                } else {
                    $window_types = [];
                    $woodType = [];
                }
                $active_window_type = '';
                foreach ($window_types as $key => $value) {
                    if (isset($this->session->data["def_image_" . $result['product_id']]) && $this->session->data["def_image_" . $result['product_id']] == $value) {
                        $active_window_type = $key;
                    }
                }
                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'calculate' => $result['calculate'],
                    'model' => $result['model'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'attribute_colors' => $attribute_colors,
                    'price' => $price,
                    'special' => $special,
                    'tax' => $tax,
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating' => $result['rating'],
                    'href' => $this->url->link('product/product', 'path=' . $sale_data['category_id'] . '&product_id=' . $result['product_id']),
                    'wood_type' => $woodType,
                    'window_types' => $window_types,
                    'active_window_type' => $active_window_type,
                    'fb_share' => 'https://www.facebook.com/sharer/sharer.php?u=' . $this->shareUrl($result['product_id'], $sale_data['category_id']),
                    'gplus_share' => 'https://plus.google.com/share?url=' . $this->shareUrl($result['product_id'], $sale_data['category_id']),
                    'vk_share' => 'http://vk.com/share.php?url' . $this->shareUrl($result['product_id'], $sale_data['category_id'])
                );
                $data['continue'] = $this->url->link('common/home');

                $data['column_left'] = $this->load->controller('common/column_left');
                $data['column_right'] = $this->load->controller('common/column_right');
                $data['content_top'] = $this->load->controller('common/content_top');
                $data['content_bottom'] = $this->load->controller('common/content_bottom');
                $data['footer'] = $this->load->controller('common/footer');
                $data['header'] = $this->load->controller('common/header');
                $this->response->setOutput($this->load->view('sales/sale', $data));
            }
        } else {
            //********************************************************************************************************************************************************************//
            //***************************List all*****************************//
            //********************************************************************************************************************************************************************//
            $sales_list = $this->model_catalog_sales->getSales();
            $this->language->get('text_home');

            $data['button_continue'] = $this->language->get('button_continue');


            foreach ($sales_list as $result) {
                // days to
                $date_to = strtotime($result['date_to']);
                $timeleft = $date_to - time();
                $daysleft = round((($timeleft / 24) / 60) / 60);
                $daysleft = $daysleft < 10 ? '0' . $daysleft : $daysleft;
                // create image
                $this->load->model('tool/image');
                if (isset($result['baner_category']) && is_file(DIR_IMAGE . $result['baner_category'])) {
                    $baner_category = $this->model_tool_image->resize($result['baner_category'], 400, 400);
                } else {
                    $baner_category = '';
                }
                // date from to
                $months = $this->language->get('months');
                $month_from = $months[date("n", strtotime($result['date_from']))];
                $day_from = date("j", strtotime($result['date_from']));
                $month_to = $months[date("n", strtotime($result['date_to']))];
                $day_to = date("j", strtotime($result['date_to']));
                $data['sales_list'][] = array(
                    'sale_href' => $this->url->link('sales/sales', 'sale_id=' . $result['sale_id']),
                    'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                    'baner_category' => $baner_category,
                    'days_left' => $this->language->get('text_left_days_to') . ' ' . $daysleft . ' ' . $this->language->get('text_days'),
                    'from_to_dates' => $this->language->get('text_from') . ' ' . $day_from . ' ' . $month_from . ' ' . $this->language->get('text_to') . ' ' . $day_to . ' ' . $month_to);
            }

            $data['continue'] = $this->url->link('common/home');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('sales/sales', $data));
        }
    }

    public function agree() {
        $this->load->model('catalog/sales');

        if (isset($this->request->get['sales_id'])) {
            $sales_id = (int) $this->request->get['sales_id'];
        } else {
            $sales_id = 0;
        }

        $output = '';

        $sales_info = $this->model_catalog_sales->getInformation($sales_id);

        if ($sales_info) {
            $output .= html_entity_decode($sales_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
        }

        $this->response->setOutput($output);
    }

    public function shareUrl($product_id = 0, $path = '') {
        if ((int) $product_id > 0) {
            $url = $this->url->link('product/product&path=' . $path . '&product_id=' . (int) $product_id);
            $url = str_replace(":", "%3A", $url);
            $url = str_replace("&", "%26", $url);
            return $url;
        } else {
            return $this->url->link('common/home');
        }
    }

}
